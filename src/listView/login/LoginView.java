package listView.login;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import listView.importer.ImportView;
import listView.root.TabView;
import listView.sale.SaleView;
import model.classes.Account;
import model.classes.Employee;
import model.classes.Importer;
import model.classes.MediaOne;
import model.classes.Salesman;
import model.wrapper.Wrapper;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class LoginView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTextField txtUserName;
  private JPasswordField txtPassword;
  private JComboBox<String> comboBox;
  private MediaOne store;

  public LoginView() {
   
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    this.setVisible(true);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 450, 310);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    setLocationRelativeTo(null);
    contentPane.setLayout(null);
    
    JLabel lblMediaOneSystem = new JLabel(b.getString("mediaOne"));
    lblMediaOneSystem.setHorizontalAlignment(SwingConstants.CENTER);
    lblMediaOneSystem.setBounds(12, 22, 426, 15);
    contentPane.add(lblMediaOneSystem);
    
    JLabel lblUser = new JLabel(b.getString("userName"));
    lblUser.setBounds(57, 85, 149, 15);
    contentPane.add(lblUser);
    
    JLabel lblNewLabel = new JLabel(b.getString("password"));
    lblNewLabel.setBounds(57, 146, 149, 15);
    contentPane.add(lblNewLabel);
    Account account = new Account();
    try {
      account = Wrapper.getAccount();
    } catch (ClassNotFoundException | IOException e2) {
      JOptionPane.showMessageDialog(null, b.getString("systemError"));
      e2.printStackTrace();
    }
    txtUserName = new JTextField();
    txtUserName.setBounds(224, 83, 142, 19);
    contentPane.add(txtUserName);
    txtUserName.setText(account.getUserName());
    txtUserName.setColumns(10);
    
    txtPassword = new JPasswordField();
    txtPassword.setBounds(224, 144, 142, 19);
    contentPane.add(txtPassword);
    txtPassword.setText(account.getPassword());
    txtPassword.setColumns(10);
    
    JButton btnLogin = new JButton(b.getString("login"));

    btnLogin.setBounds(57, 232, 117, 25);
    contentPane.add(btnLogin);
    
    JCheckBox chckbxSave = new JCheckBox(b.getString("save"));
    if (!account.getUserName().equals("")) {
      chckbxSave.setSelected(true);
    }
    chckbxSave.setBounds(252, 184, 129, 23);
    contentPane.add(chckbxSave);
    
    comboBox = new JComboBox<>();
    comboBox.setModel(new DefaultComboBoxModel<>(new String[] {b.getString("root"), b.getString("salesman"), b.getString("importer")}));
    comboBox.setBounds(57, 183, 117, 24);
    contentPane.add(comboBox);
    
    // handle event below
    //
    //
    
    // handle login event
    btnLogin.addActionListener(new ActionListener() {
      
      @SuppressWarnings("deprecation")
      @Override
      public void actionPerformed(ActionEvent e) {
        if (txtUserName.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("userName"));
          return;
        }
        
        if (txtPassword.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("password"));
          return;
        }
        int index = comboBox.getSelectedIndex();
        Account account = new Account();
        account.setUserName(txtUserName.getText());
        account.setPassword(txtPassword.getText());
        
        // Load data 

       
        try {
          store = Wrapper.getStore();
          ArrayList<Employee> employees = Wrapper.getEmployees();
          store.setEmployees(employees);
        } catch (ClassNotFoundException | IOException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
        Employee employee;
        if (index == 0) {
          if (store.getRootAccount().equals(account)) {
            frame.dispose();
            new TabView();
          } else {
            JOptionPane.showMessageDialog(null, b.getString("wrongAccount"));

          }
        } else if (index == 1) {
          try {
            employee = store.getEmployeeByAccount(account);
            if (employee instanceof Salesman) {
              Wrapper.saveSalesman((Salesman) employee);
              frame.dispose();
              new SaleView();
            } else {
              JOptionPane.showMessageDialog(null, b.getString("wrongAccount"));

            }
          } catch (Exception e1) {
            JOptionPane.showMessageDialog(null, b.getString("wrongAccount"));
            e1.printStackTrace();
          }
        } else if (index == 2) {
          try {
            employee = store.getEmployeeByAccount(account);
            if (employee instanceof Importer) {
              Wrapper.saveImporter((Importer) employee);
              frame.dispose();
              new ImportView();
            } else {
              JOptionPane.showMessageDialog(null, b.getString("wrongAccount"));

            }
          } catch (Exception e1) {
            JOptionPane.showMessageDialog(null, b.getString("wrongAccount"));
            e1.printStackTrace();
          }
        }
        
        saveAccount(account);
      }
     
      
      private void saveAccount(Account account) {
        if (chckbxSave.isSelected()) {
          try {
            Wrapper.saveAccount(account);
          } catch (IOException e) {
           
            JOptionPane.showMessageDialog(null, b.getString("systemError"));
          }
        } else {
          Account acc = new Account();
          acc.setUserName("");
          acc.setPassword("");
          try {
            Wrapper.saveAccount(acc);
          } catch (IOException e) {

            JOptionPane.showMessageDialog(null, b.getString("systemError"));
       
          }
        }
      }
    });
  }
}
