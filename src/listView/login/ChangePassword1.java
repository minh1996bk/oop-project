package listView.login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import model.classes.Account;
import model.classes.Employee;
import model.classes.Importer;
import model.classes.MediaOne;
import model.classes.Salesman;
import model.wrapper.NoAppriciateResultException;
import model.wrapper.Wrapper;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

public class ChangePassword1 extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JPasswordField txtPassword;
  private JPasswordField txtNewPassword;
  private JPasswordField txtNewPassword1;
  private JTextField txtNewUserName;
  private TypeEmployee type;

  public ChangePassword1(TypeEmployee typeEmployee) {
    this.type = typeEmployee;
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    setVisible(true);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 450, 300);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
    
    JLabel lblPassword = new JLabel(b.getString("password"));
    lblPassword.setBounds(38, 70, 108, 20);
    contentPane.add(lblPassword);
    
    JLabel lblNewPassword = new JLabel(b.getString("newPassword"));
    lblNewPassword.setBounds(38, 120, 108, 20);
    contentPane.add(lblNewPassword);
    
    JLabel lblNewPassword_1 = new JLabel(b.getString("newPassword"));
    lblNewPassword_1.setBounds(38, 170, 108, 20);
    contentPane.add(lblNewPassword_1);
    
    txtPassword = new JPasswordField();
    txtPassword.setBounds(179, 70, 200, 20);
    contentPane.add(txtPassword);
    txtPassword.setColumns(10);
    
    txtNewPassword = new JPasswordField();
    txtNewPassword.setBounds(179, 120, 200, 20);
    contentPane.add(txtNewPassword);
    txtNewPassword.setColumns(10);
    
    txtNewPassword1 = new JPasswordField();
    txtNewPassword1.setBounds(179, 170, 200, 20);
    contentPane.add(txtNewPassword1);
    txtNewPassword1.setColumns(10);
    
    JButton btnSave = new JButton(b.getString("save"));
    btnSave.setBounds(38, 214, 117, 25);
    contentPane.add(btnSave);
    
    JLabel lblUsername = new JLabel(b.getString("userName"));
    lblUsername.setBounds(38, 38, 108, 15);
    contentPane.add(lblUsername);
    
    txtNewUserName = new JTextField();
    txtNewUserName.setBounds(178, 39, 201, 19);
    contentPane.add(txtNewUserName);
    txtNewUserName.setColumns(10);
    
    JButton btnBack = new JButton(b.getString("back")); 
    btnBack.setBounds(183, 214, 117, 25);
    contentPane.add(btnBack);
    
    // handle event below
    
    //
    //
    //
    // handle save new account event
    
    btnSave.addActionListener(new ActionListener() {
      
      private MediaOne store;

      @SuppressWarnings("deprecation")
      @Override
      public void actionPerformed(ActionEvent e) {
        String password = txtPassword.getText();
        String newPwd = txtNewPassword.getText();
        String newPwd1 = txtNewPassword1.getText();
        String newUserName = txtNewUserName.getText();
        if (newUserName.equals("")
            ||password.equals("") 
            || newPwd.equals("")
            || newPwd1.equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("fillAlls"));
          return;
        }
        if (!newPwd.equals(newPwd1)) {
          JOptionPane.showMessageDialog(null, b.getString("fillSame"));
          return;
        }

        try {
          store = Wrapper.getStore();
          ArrayList<Employee> employees = Wrapper.getEmployees();
          store.setEmployees(employees);
        } catch (ClassNotFoundException | IOException e2) {
          // TODO Auto-generated catch block
          e2.printStackTrace();
        }

        if (type.toString().equals(TypeEmployee.SALESMAN.toString())) {
          try {
          
  
            Salesman salesman = Wrapper.getSalesman();
            if (salesman.getAccount().getPassword().equals(password)) {
              Employee employee = store.searchEmployee(salesman.getIdEmployee());
              employee.setAccount(new Account(newUserName, newPwd));
              
              Wrapper.saveEmployees(store.getEmployees());
              Wrapper.saveSalesman((Salesman) employee);
              
              JOptionPane.showMessageDialog(null, b.getString("ok"));
              frame.dispose();
            } else {
              JOptionPane.showMessageDialog(null, b.getString("wrongAccount"));
            }
            

            
          } catch (ClassNotFoundException | IOException | NoAppriciateResultException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
          }
        } else if (type.toString().equals(TypeEmployee.IMPORTER.toString())) {
          try {

            Importer importer = Wrapper.getImporter();
            if (importer.getAccount().getPassword().equals(password)) {
              
              Employee employee = store.searchEmployee(importer.getIdEmployee());
              employee.setAccount(new Account(newUserName, newPwd));
              
              Wrapper.saveEmployees(store.getEmployees());
              Wrapper.saveImporter((Importer) employee);
              
              JOptionPane.showMessageDialog(null, b.getString("ok"));
              frame.dispose();

            } else {
              JOptionPane.showMessageDialog(null, b.getString("wrongAccount"));
            }
        
            
            
          } catch (ClassNotFoundException | IOException | NoAppriciateResultException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
          }
        } else if (type.toString().equals(TypeEmployee.ROOT.toString())) {
          try {
            store.setRootAccount(new Account(newUserName, newPwd1));
            Wrapper.saveStore(store);
            
            JOptionPane.showMessageDialog(null, b.getString("ok"));
            frame.dispose();
          } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
          }
        }
      }
    });
    btnBack.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
        
      }
    });
  }
}