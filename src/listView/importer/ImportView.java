package listView.importer;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import listView.login.ChangePassword1;
import listView.login.TypeEmployee;
import listView.root.MyActionListener;
import listView.sale.Clock;
import model.classes.Book;
import model.classes.BookGenre;
import model.classes.Booth;
import model.classes.Employee;
import model.classes.Film;
import model.classes.FilmGenre;
import model.classes.ImportReceipt;
import model.classes.Importer;
import model.classes.MediaOne;
import model.classes.MusicAlbum;
import model.classes.MusicGenre;
import model.classes.Product;
import model.classes.ProductItem;
import model.classes.Supplier;
import model.wrapper.NoAppriciateResultException;
import model.wrapper.Wrapper;

import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class ImportView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTable table;

  private JPanel inputPanel;
  private JTextField txtName;
  private JTextField txtPrice;
  private JTextField txtAuthor;
  private JTextField txtContents;
  private JComboBox<String> genreBox;
  private JTextField txtAmount;
  private JTextField txtImportPrice;
  private JLabel lblAuSingActor;
  private JLabel lblContentCom;
  private MediaOne mediaOne;
  private DefaultTableModel model;

  public ImportView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;  
    mediaOne = new MediaOne();
    try {
      mediaOne = Wrapper.getStore();
      ArrayList<Booth> booths = Wrapper.getBooths();
      mediaOne.setBooths(booths);

      
      
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    setTitle(b.getString("import") + " " + b.getString("product"));
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 683, 700);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    setVisible(true);
    setLocationRelativeTo(null);
    contentPane.setLayout(new BorderLayout(0, 0));
    
    JToolBar toolBar = new JToolBar();
    contentPane.add(toolBar, BorderLayout.SOUTH);
    
    JLabel lblDisplayTiem = new JLabel();
    toolBar.add(lblDisplayTiem);
    Clock myClock = new Clock(lblDisplayTiem);
    myClock.start();
    
    JPanel panel = new JPanel();
    contentPane.add(panel, BorderLayout.WEST);
    
    JScrollPane scrollPane = new JScrollPane();
    contentPane.add(scrollPane, BorderLayout.CENTER);
    
    table = new JTable();
    String[] columnNames = {b.getString("name"), b.getString("number"), b.getString("importPrice")};
    model = new DefaultTableModel(columnNames, 0);
    table.setModel(model);
    scrollPane.setViewportView(table);
    
    JPanel northPanel = new JPanel();
    contentPane.add(northPanel, BorderLayout.NORTH);
    northPanel.setLayout(new BorderLayout(0, 0));
    
    JLabel lblNewLabel = new JLabel(b.getString("import") + " " + b.getString("product"));
    lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 20));
    lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
    northPanel.add(lblNewLabel, BorderLayout.NORTH);
    
    inputPanel = new JPanel();
    northPanel.add(inputPanel, BorderLayout.CENTER);
 
    inputPanel.setLayout(new GridLayout(10, 2, 10, 10));
    inputPanel.setBorder(new EmptyBorder(20, 20, 0, 0));

    JLabel lblNewLabel_2 = new JLabel(b.getString("name"));
    inputPanel.add(lblNewLabel_2);
    
    txtName = new JTextField();
    inputPanel.add(txtName);
    txtName.setColumns(10);
    
    JLabel lblNewLabel_3 = new JLabel(b.getString("price"));
    inputPanel.add(lblNewLabel_3);
    
    txtPrice = new JTextField();
    inputPanel.add(txtPrice);
    txtPrice.setColumns(10);
    
    lblAuSingActor = new JLabel(b.getString("singer"));
    inputPanel.add(lblAuSingActor);
    
    txtAuthor = new JTextField();
    inputPanel.add(txtAuthor);
    txtAuthor.setColumns(10);
    
    lblContentCom = new JLabel(b.getString("composer"));
    inputPanel.add(lblContentCom);
    
    txtContents = new JTextField();
    inputPanel.add(txtContents);
    txtContents.setColumns(10);
    
    JLabel lblNewLabel_6 = new JLabel(b.getString("type"));
    inputPanel.add(lblNewLabel_6);
    
    genreBox = new JComboBox<>();
    
    ArrayList<MusicGenre> genres = mediaOne.getMusicGenres();
    String[] array = new String[genres.size()];
    for (int i = 0; i < genres.size(); i ++) {
      array[i] = genres.get(i).getName();
    }
    genreBox.setModel(new DefaultComboBoxModel<>(array));
    inputPanel.add(genreBox);
    
    JLabel lblNewLabel_7 = new JLabel(b.getString("number"));
    inputPanel.add(lblNewLabel_7);
    
    txtAmount = new JTextField();
    inputPanel.add(txtAmount);
    txtAmount.setColumns(10);
    
    JLabel lblNewLabel_8 = new JLabel(b.getString("importPrice"));
    inputPanel.add(lblNewLabel_8);
    
    txtImportPrice = new JTextField();
    inputPanel.add(txtImportPrice);
    txtImportPrice.setColumns(10);
    
    JPanel btnPanel = new JPanel();
    northPanel.add(btnPanel, BorderLayout.SOUTH);
    
    JButton btnInsert = new JButton(b.getString("insert"));
    
    btnPanel.add(btnInsert);
    
    JButton btnDelete = new JButton(b.getString("delete"));
   
    btnPanel.add(btnDelete);
    
    JButton btnCreate = new JButton(b.getString("create"));
    btnPanel.add(btnCreate);
    
    JButton btnExit = new JButton(b.getString("exit"));
    
    btnPanel.add(btnExit);
    
    JButton btnChange = new JButton(b.getString("change") + " " + b.getString("password"));
    btnPanel.add(btnChange);
    
    JPanel leftPanel = new JPanel();
    northPanel.add(leftPanel, BorderLayout.WEST);
    leftPanel.setLayout(new GridLayout(10, 1, 0, 10));
    
    JLabel lblSelectSupplier = new JLabel(b.getString("sellect") + " " + b.getString("supplier"));
    leftPanel.add(lblSelectSupplier);
    
    JComboBox<String> comboBox = new JComboBox<>();
    ArrayList<Supplier> supplierList = mediaOne.getSuppliers();
    int size = supplierList.size();
    String[] suppliers = new String[size];
    for (int i = 0; i < size; i ++) {
      suppliers[i] = "" + supplierList.get(i).getIdSupplier();
    }
    comboBox.setModel(new DefaultComboBoxModel<>(suppliers));
   
   
    leftPanel.add(comboBox);
  
    JLabel lblSelectType = new JLabel(b.getString("sellect") + " " + b.getString("type"));
    leftPanel.add(lblSelectType);
    
    JComboBox<String> productType = new JComboBox<>();
    productType.setModel(new DefaultComboBoxModel<>(new String[] {b.getString("musicCD"), b.getString("book"), b.getString("filmCD")}));
    
    leftPanel.add(productType);
    
    JLabel lblTotal = new JLabel(b.getString("total"));
    leftPanel.add(lblTotal);
    
    JTextField txtTotal = new JTextField("0");
    txtTotal.setEditable(false);
    leftPanel.add(txtTotal);
    
    JLabel lblBooth = new JLabel(b.getString("booth"));
    leftPanel.add(lblBooth);
    
    ArrayList<Booth> booths = mediaOne.getBooths();
    Integer[] boothNames = new Integer[booths.size()];
    for (int i = 0; i < booths.size(); i ++) {
      boothNames[i] = booths.get(i).getName();
    }
    JComboBox<Integer> cmbBooth = new JComboBox<>(boothNames);
    leftPanel.add(cmbBooth);
    
    // event handle
    //
    //
    //
    //
    //
    // handle insert event
    btnInsert.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (!preProcess()) {
          return;
        }
        int index = productType.getSelectedIndex();
        try {
          if (index == 0) {
            if (!processMusic()) {
              return;
            }
          } else if (index == 1) {
            if (!processBook()) {
              return;
            }
          } else {
            if (!processFilm()) {
              return;
            }
          }
        } catch (ArrayIndexOutOfBoundsException ex) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          return;
        }
        display();
        
      }
      
      private boolean preProcess() {
        if (txtName.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("name"));
          return false;
        }
        
        if (txtPrice.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("price"));
          return false;
        }
        
       
        
        if (txtAuthor.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("singer"));
          return false;
        }
        
        if (txtContents.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("composer"));
          return false;
        }
        
        if (txtAmount.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("number"));
          return false;
        }
        
        if (txtImportPrice.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("importPrice"));
          return false;
        }
        
        try {
          Integer.parseInt(txtPrice.getText());
          Integer.parseInt(txtAmount.getText());
          Integer.parseInt(txtImportPrice.getText());
        } catch (NumberFormatException ex) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("numericData"));
          return false;
        }
        return true;
      }
      
      private void display() {
        String[] row = {txtName.getText(), txtAmount.getText(), txtImportPrice.getText()};
        model.addRow(row);
        int total = Integer.parseInt(txtTotal.getText());
        total += Integer.parseInt(txtAmount.getText()) * Integer.parseInt(txtImportPrice.getText());
        txtTotal.setText("" + total);
        txtName.setText(null);
        txtPrice.setText(null);
        txtAuthor.setText(null);
        txtContents.setText(null);
        txtAmount.setText(null);
        txtImportPrice.setText(null);
        
      }
      
      private boolean processMusic() {
        try {
          // Load data below
          ArrayList<ProductItem> listItems = Wrapper.getListItems();
          // Load data above
          MusicAlbum music = new MusicAlbum();
          music.setProductName(txtName.getText());
          music.setPrice(Integer.parseInt(txtPrice.getText()));
          music.setSinger(txtAuthor.getText());
          music.setComposer(txtContents.getText());
          music.setMusicGenre(mediaOne.getMusicGenres().get(genreBox.getSelectedIndex()));
          Importer.insertItemToListItem(listItems, music, Integer.parseInt(txtAmount.getText()), Integer.parseInt(txtTotal.getText()));
          
          // Save data below
          Wrapper.saveListItem(listItems);
          // Save data above
          
          return true;
        } catch (ClassNotFoundException e) {
          JOptionPane.showMessageDialog(null, e);
          return false;
        } catch (IOException e) {
          JOptionPane.showMessageDialog(null, e);
          return false;
        } catch (NumberFormatException e) {
          e.printStackTrace();
          return false;
        } catch (Exception e) {

          e.printStackTrace();
          return false;
        }
   
      }
      
      private boolean processBook() {
        try {
          // Load data below
          ArrayList<ProductItem> listItems = Wrapper.getListItems();
          // Load data above
          Book book = new Book();
          book.setProductName(txtName.getText());
          book.setPrice(Integer.parseInt(txtPrice.getText()));
          book.setAuthor(txtAuthor.getText());
          book.setSummaryContents(txtContents.getText());
          book.setBookGenre(mediaOne.getBookGenres().get(genreBox.getSelectedIndex()));
          Importer.insertItemToListItem(listItems, book, Integer.parseInt(txtAmount.getText()), Integer.parseInt(txtTotal.getText()));
          
          // Save data below
          Wrapper.saveListItem(listItems);
          // Save data above
          
          return true;
        } catch (ClassNotFoundException e) {
          JOptionPane.showMessageDialog(null, e);
          return false;
        } catch (IOException e) {
          JOptionPane.showMessageDialog(null, e);
          return false;
        } catch (NumberFormatException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          return false;
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          return false;
        }
        
       
      }
      
      private boolean processFilm() {
        try {
          // Load data below
          ArrayList<ProductItem> listItems = Wrapper.getListItems();
          // Load data above
          Film film = new Film();
          film.setProductName(txtName.getText());
          film.setPrice(Integer.parseInt(txtPrice.getText()));
          film.setActorNames(txtAuthor.getText());
          film.setSummaryContents(txtContents.getText());
          film.setFilmGenre(mediaOne.getFilmGenres().get(genreBox.getSelectedIndex()));
          Importer.insertItemToListItem(listItems, film, Integer.parseInt(txtAmount.getText()), Integer.parseInt(txtTotal.getText()));
          
          // Save data below
          Wrapper.saveListItem(listItems);
          // Save data above
          
          return true;
        } catch (ClassNotFoundException e) {
          JOptionPane.showMessageDialog(null, e);
          return false;
        } catch (IOException e) {
          JOptionPane.showMessageDialog(null, e);
          return false;
        } catch (NumberFormatException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          return false;
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          return false;
        }
        
        
      }
    });
    
    // handle create receipt event
    btnCreate.addActionListener(new MyActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (!preProcess()) {
          JOptionPane.showMessageDialog(null, b.getString("empty"));
          return;
        }
        
        if (process()) {
          JOptionPane.showMessageDialog(null, b.getString("ok"));
          
        } else {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          ArrayList<ProductItem> listItems = new ArrayList<>();
          try {
            Wrapper.saveListItem(listItems);
          } catch (IOException e1) {
            e1.printStackTrace();
          }
          
        }
        
        display();
      }
      
      @Override
      public boolean preProcess() {
        if (model.getRowCount() == 0) {
          return false;
        } else {
          return true;
        }
        
      }
      
      @Override
      public boolean process() {
        try {
          // Load data below
          ArrayList<ProductItem> listItems = Wrapper.getListItems();
          
          MediaOne store = Wrapper.getStore();
          ArrayList<Product> products = Wrapper.getProducts();
          ArrayList<ImportReceipt> imReceipts = Wrapper.getImportReceipt();
          ArrayList<Booth> booths = Wrapper.getBooths();
          ArrayList<Employee> employees = Wrapper.getEmployees();
          store.setProducts(products);
          store.setImportReceipts(imReceipts);
          store.setBooths(booths);
          store.setEmployees(employees);
          Importer importer = Wrapper.getImporter();
          // Load data above
          
          Supplier supplier = store.getSuppliers().get(comboBox.getSelectedIndex());
        
          ImportReceipt receipt = importer.makeImportReceipt(store, supplier, listItems, (Integer) cmbBooth.getSelectedItem(), Integer.parseInt(txtTotal.getText()));

          // Print receipt
          Wrapper.printImportReceipt(receipt);
          
          
          // Save data below
          listItems = new ArrayList<>();
          Wrapper.saveListItem(listItems);
          Wrapper.saveStore(store);
          Wrapper.saveProducts(products);
          Wrapper.saveImportReceipt(imReceipts);
          Wrapper.saveBooths(booths);
          Wrapper.saveEmployees(employees);
          // Save data above
          
          return true;
        } catch (ClassNotFoundException | IOException | NoAppriciateResultException e) {
          e.printStackTrace();
          JOptionPane.showMessageDialog(null, e);
          return false;
        }
      }
      
      @Override
      public void display() {
        model.setRowCount(0);
        txtTotal.setText("0");
        txtName.setText(null);
        txtPrice.setText(null);
        txtAuthor.setText(null);
        txtContents.setText(null);
        txtAmount.setText(null);
        txtImportPrice.setText(null);
        
      }
    });
    
    // handle product type comboBox's item changed event
    
    productType.addItemListener(new ItemListener() {
          
          @Override
          public void itemStateChanged(ItemEvent arg0) {
            int typeIndex = productType.getSelectedIndex();
            if (typeIndex == 0) {
              lblAuSingActor.setText(b.getString("singer"));
              lblContentCom.setText(b.getString("composer"));
              
              ArrayList<MusicGenre> genres = mediaOne.getMusicGenres();
              String[] array = new String[genres.size()];
              for (int i = 0; i < genres.size(); i ++) {
                array[i] = genres.get(i).getName();
              }
              genreBox.setModel(new DefaultComboBoxModel<>(array));
              genreBox.setToolTipText(genres.get(productType.getSelectedIndex()).getDescription());
            } else if (typeIndex == 1) {
              lblAuSingActor.setText(b.getString("author"));
              lblContentCom.setText(b.getString("content"));
              
              ArrayList<BookGenre> genres = mediaOne.getBookGenres();
              String[] array = new String[genres.size()];
              for (int i = 0; i < genres.size(); i ++) {
                array[i] = genres.get(i).getName();
              }
              genreBox.setModel(new DefaultComboBoxModel<>(array));
            
            } else {
              lblAuSingActor.setText(b.getString("actors"));
              lblContentCom.setText(b.getString("content"));
              
              ArrayList<FilmGenre> genres = mediaOne.getFilmGenres();
              String[] array = new String[genres.size()];
              for (int i = 0; i < genres.size(); i ++) {
                array[i] = genres.get(i).getName();
              }
              genreBox.setModel(new DefaultComboBoxModel<>(array));
            }
          }
        });
     
    // handle delete item event
     btnDelete.addActionListener(new MyActionListener() {
          
          @Override
          public void actionPerformed(ActionEvent arg0) {
            if (!preProcess()) {
              return;
            }
            if (process()) {
              display();
            } else {
              JOptionPane.showMessageDialog(null, b.getString("systemError"));
              return;
            }
            
          }
          
          @Override
          public boolean process() {  
          
            try {
              // Load data below
              ArrayList<ProductItem> listItems = Wrapper.getListItems();
              // Load data above
              
              Importer.deleteItemFromListItem(listItems, table.getSelectedRow());
              
              // Save data below
              Wrapper.saveListItem(listItems);
              // Save data above
              
              return true;
            } catch (ClassNotFoundException | IOException e1) {
              JOptionPane.showMessageDialog(northPanel, "systemError");
              return false;
            }
      
          }
          
          @Override
          public boolean preProcess() {
            if (table.getSelectedRow() == -1) {
              return false;
            }
            return true;
          }
          
          @Override
          public void display() {
            model.removeRow(table.getSelectedRow());
          }
    });
     
     // handle cancel event
     btnExit.addActionListener(new MyActionListener() {
       
       @Override
       public void actionPerformed(ActionEvent arg0) {
         if (preProcess()) {
           process();
           display();
         } else {
           return;
         }
       }
       
       @Override
       public boolean preProcess() {
         int index = JOptionPane.showConfirmDialog(null, b.getString("exit") + " y/n?");
         if (index == 0) {
           return true;
         } else {
           return false;
         }
       }
       
       @Override
       public boolean process() {
         try {
           // Reset data 
           ArrayList<ProductItem> listItems = new ArrayList<>();
           
           // Save data below
           Wrapper.saveListItem(listItems);
           return true;
         } catch (IOException e) {
           return false;
         }
       }
       
       @Override
       public void display() {
         frame.dispose();
         
         
       }
     });
     
     btnChange.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        new ChangePassword1(TypeEmployee.IMPORTER);
      }
    });
  }
  
    

  
}
