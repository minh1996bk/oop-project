package listView.sale;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import listView.root.MyModel;
import listView.root.ProductInfoView;
import model.classes.Book;
import model.classes.Booth;
import model.classes.Film;
import model.classes.MediaOne;
import model.classes.MusicAlbum;
import model.classes.Product;
import model.classes.ProductItem;
import model.wrapper.NoAppriciateResultException;
import model.wrapper.Wrapper;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;

public class SearchProduct extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTable table;
  private JTextField textField;
  private MediaOne store;
  private JComboBox<String> comboBox;
  private DefaultComboBoxModel<String> cmbModel;
  private DefaultTableModel model;
  
  public SearchProduct() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    frame.setVisible(true);
    setTitle(b.getString("search"));
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 667, 391);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    setLocationRelativeTo(null);
    contentPane.setLayout(new BorderLayout(0, 0));
    try {
      store = Wrapper.getStore();
      ArrayList<Booth> booths = Wrapper.getBooths();
      store.setBooths(booths);
    } catch(ClassNotFoundException e) {
      JOptionPane.showMessageDialog(null, b.getString("systemError"));
    } catch (IOException e1) {
      JOptionPane.showMessageDialog(null, b.getString("systemError"));
    }
    
    JPanel panel = new JPanel();
    contentPane.add(panel, BorderLayout.NORTH);
    panel.setLayout(new GridLayout(0, 2, 10, 10));
    
    JComboBox<String> cmbProductType = new JComboBox<>();
    cmbProductType.setModel(new DefaultComboBoxModel<>(new String[] {b.getString("musicCD"), b.getString("book"), b.getString("filmCD")}));
    cmbProductType.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        int index = cmbProductType.getSelectedIndex();
        if (index == 0) {
          cmbModel = new DefaultComboBoxModel<>(store.getMusicGenreToString());
          comboBox.setModel(cmbModel);
        } else if (index == 1) {
          cmbModel = new DefaultComboBoxModel<>(store.getBookGenreToString());
          comboBox.setModel(cmbModel);
        } else {
          cmbModel = new DefaultComboBoxModel<>(store.getFilmGenreToString());
          comboBox.setModel(cmbModel);
        }
      }
    });
    panel.add(cmbProductType);
    
    comboBox = new JComboBox<>();
    cmbModel = new DefaultComboBoxModel<>(store.getMusicGenreToString());
    comboBox.setModel(cmbModel);
    panel.add(comboBox);
    
    textField = new JTextField();
    panel.add(textField);
    textField.setColumns(10);
    
    JButton btnSearch = new JButton(b.getString("search"));
    btnSearch.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        String genre = (String) comboBox.getSelectedItem();
        String searchPattern = textField.getText();
        LinkedHashMap<ProductItem, Booth> map = store.searchProductItemAndCorrespondBooth(searchPattern, genre);
        String[] columnNames = {b.getString("id") , b.getString("name"), b.getString("number"), b.getString("booth")};
        model = new MyModel(columnNames);
        Product product;
        if (map.isEmpty()) {
          model.addRow(new String[] {b.getString("nodata")});
          table.setModel(model);

          return;
        }
        for (Map.Entry<ProductItem, Booth> pair : map.entrySet()) {
          product = pair.getKey().getProduct();
          String[] row = {"" + product.getIdProduct(), product.getProductName(), "" + pair.getKey().getAmount(), "" + pair.getValue().getName()};
          model.addRow(row);    
        }
          
        table.setModel(model);
      }
    });
    panel.add(btnSearch);
    
    JScrollPane scrollPane = new JScrollPane();
    contentPane.add(scrollPane, BorderLayout.CENTER);
    
    table = new JTable();
    scrollPane.setViewportView(table);
    
    table.addMouseListener(new MouseListener() {
      
      @Override
      public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
        if (e.getClickCount() != 2) {
          return;
        }
        int rowIndex = table.getSelectedRow();
      
        
        String stringId = (String) table.getModel().getValueAt(rowIndex, 0);
        int productId;
        try {
          productId = Integer.parseInt(stringId);
        } catch (NumberFormatException e1) {
          return;
        }
        
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Product> products = Wrapper.getProducts();
          store.setProducts(products);
          // Load data above
          
          Product product = store.searchProductById(productId);
          ProductInfoView view = new ProductInfoView();
          view.getBtnSaleOff().setEnabled(false);
          view.getBtnSave().setEnabled(false);
          view.getTxtName().setEditable(false);
          view.getTxtPrice().setEditable(false);
          view.getTxtOp1().setEditable(false);
          view.getTxtOp2().setEditable(false);
          view.getTxtOp3().setEditable(false);
          if (product instanceof MusicAlbum) {
            MusicAlbum music = (MusicAlbum) product;
            view.getTxtId().setText("" + music.getIdProduct());
            view.getTxtName().setText(music.getProductName());
            view.getTxtPrice().setText("" + music.getPrice());
            view.getLblOp1().setText(b.getString("musicGenre"));
            view.getTxtOp1().setText(music.getMusicGenre().getName());
            view.getLblOp2().setText(b.getString("singer"));
            view.getTxtOp2().setText(music.getSinger());
            view.getLblOp3().setText(b.getString("composer"));
            view.getTxtOp3().setText(music.getComposer());
          } else if (product instanceof Book) {
            Book book = (Book) product;
            view.getTxtId().setText("" + book.getIdProduct());
            view.getTxtName().setText(book.getProductName());
            view.getTxtPrice().setText("" + book.getPrice());
            view.getLblOp1().setText(b.getString("bookGenre"));
            view.getTxtOp1().setText(book.getBookGenre().getName());
            view.getLblOp2().setText(b.getString("content"));
            view.getTxtOp2().setText(book.getSummaryContents());
            view.getLblOp3().setText(b.getString("author"));
            view.getTxtOp3().setText(book.getAuthor());
          } else if (product instanceof Film) {
            Film film = (Film) product;
            view.getTxtId().setText("" + film.getIdProduct());
            view.getTxtName().setText(film.getProductName());
            view.getTxtPrice().setText("" + film.getPrice());
            view.getLblOp1().setText(b.getString("filmGenre"));
            view.getTxtOp1().setText(film.getFilmGenre().getName());
            view.getLblOp2().setText(b.getString("actors"));
            view.getTxtOp2().setText(film.getActorNames());
            view.getLblOp3().setText(b.getString("content"));
            view.getTxtOp3().setText(film.getSummaryContents());
          }
        } catch (ClassNotFoundException | IOException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e1.printStackTrace();
        } catch (NoAppriciateResultException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e1.printStackTrace();
        }
        
      }
    });
    
    
    JPanel panel_1 = new JPanel();
    contentPane.add(panel_1, BorderLayout.SOUTH);
    
    JButton btnBack = new JButton(b.getString("back"));
    btnBack.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
        new SaleView();
      }
    });
    panel_1.add(btnBack);
  }

}
