package listView.sale;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import listView.login.ChangePassword1;
import listView.login.TypeEmployee;
import listView.root.MyActionListener;
import model.classes.Booth;
import model.classes.Employee;
import model.classes.ExportReceipt;
import model.classes.MediaOne;
import model.classes.Product;
import model.classes.ProductItem;
import model.classes.Salesman;
import model.wrapper.NoAppriciateResultException;
import model.wrapper.Wrapper;

public class SaleView extends JFrame {

  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTextField txtId;
  private JTextField txtAmount;
  private JTable table;
  private DefaultTableModel model;
  private JTextField txtTotal;


  public SaleView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    setVisible(true);
    setTitle(b.getString("sale") + " " + b.getString("product"));
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 700, 500);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    setLocationRelativeTo(null);
    contentPane.setLayout(new BorderLayout(0, 0));
    
    JToolBar toolBar = new JToolBar();
    contentPane.add(toolBar, BorderLayout.SOUTH);
    
    JLabel lblTimeDisplayedHere = new JLabel();
    Clock myClock = new Clock(lblTimeDisplayedHere);
    myClock.start();
    lblTimeDisplayedHere.setHorizontalAlignment(SwingConstants.LEFT);
    toolBar.add(lblTimeDisplayedHere);
    
    JLabel lblSale = new JLabel(b.getString("sale"));
    lblSale.setHorizontalAlignment(SwingConstants.CENTER);
    contentPane.add(lblSale, BorderLayout.NORTH);
    
    JPanel panel = new JPanel();
    contentPane.add(panel, BorderLayout.WEST);
    panel.setLayout(new GridLayout(10, 1, 10, 10));
    
    JLabel lblId = new JLabel(b.getString("id"));
    panel.add(lblId);
    
    txtId = new JTextField();
    panel.add(txtId);
    txtId.setColumns(10);
    
    JLabel lblAmount = new JLabel(b.getString("number"));
    panel.add(lblAmount);
    
    txtAmount = new JTextField();
    panel.add(txtAmount);
    txtAmount.setColumns(10);
    
    JButton btnInsert = new JButton(b.getString("insert"));

    panel.add(btnInsert);
    
    JLabel lblToTal = new JLabel(b.getString("total"));
    panel.add(lblToTal);
    
    txtTotal = new JTextField("0");
    txtTotal.setEditable(false);
    panel.add(txtTotal);
    
    JPanel panel_1 = new JPanel();
    contentPane.add(panel_1, BorderLayout.EAST);
    panel_1.setLayout(new GridLayout(10, 1, 10, 10));
    
    JButton btnDelete = new JButton(b.getString("delete"));
    panel_1.add(btnDelete);

    JButton btnCreate = new JButton(b.getString("create"));

    panel_1.add(btnCreate);
    
    
    JButton btnSearch = new JButton(b.getString("search"));

    panel_1.add(btnSearch);
    
    JButton btnChange = new JButton(b.getString("change") + " " + b.getString("password"));
    panel_1.add(btnChange);
    
    JButton btnCancel = new JButton(b.getString("exit"));
 
    panel_1.add(btnCancel);

    
    JScrollPane scrollPane = new JScrollPane();
    contentPane.add(scrollPane, BorderLayout.CENTER);
    scrollPane.setBorder(new EmptyBorder(20, 20, 20, 20));
    
    table = new JTable();
    String[] columnNames = {b.getString("id"), b.getString("name"), b.getString("number"), b.getString("price"), b.getString("saleOff")};
    model = new DefaultTableModel(columnNames, 0);
    table.setModel(model);
    scrollPane.setViewportView(table);
    
    // handle events below
    //
    //
    btnInsert.addActionListener(new MyActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (!preProcess()) {
          return;
        }
        
        process();
        
        
      }
      
      @Override
      public boolean process() {
       
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Product> products = Wrapper.getProducts();
          ArrayList<Booth> booths = Wrapper.getBooths();
          store.setProducts(products);
          store.setBooths(booths);
          ArrayList<ProductItem> listItems = Wrapper.getListItems();
          // Load data above  
          int idProduct = Integer.parseInt(txtId.getText());
          int amount = Integer.parseInt(txtAmount.getText()); 
          Salesman.insertItemToListItem(store, listItems, idProduct, amount);
          // Save data below
          Wrapper.saveListItem(listItems);
          // Save data above
          // display
          ProductItem newItem = listItems.get(listItems.size() -1);
          display(newItem);
          return true;
        } catch (ClassNotFoundException | IOException e) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          return false;
        } catch (NoAppriciateResultException e) {
          JOptionPane.showMessageDialog(null, b.getString("invalidID"));
          
          return false;
        } catch (Exception e) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          return false;
        }
       
      
     
        
        
      }
      
      @Override
      public boolean preProcess() {
        if (txtId.getText().equals("")
            || txtAmount.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("fillAlls"));
          return false;
        }
        try {
          Integer.parseInt(txtId.getText());
          Integer.parseInt(txtAmount.getText());
        } catch (NumberFormatException ex) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " +  b.getString("numericData"));
          return false;
        }
        return true;
      }
      
      @Override
      public void display() {
        String[] row = {txtId.getText(), txtAmount.getText()};
        model.addRow(row);
        txtId.setText(null);
        txtAmount.setText(null);
        
        
      }
      
      private void display(ProductItem item) {
        String[] row = {txtId.getText(), item.getProduct().getProductName(), txtAmount.getText(),"" + item.getProduct().getPrice(), "" + item.getPrice()};
        model.addRow(row);
        txtId.setText(null);
        txtAmount.setText(null);
        int preTotal = Integer.parseInt(txtTotal.getText());
        int currentTotal = preTotal + item.getAmount() * item.getPrice();
        txtTotal.setText("" + currentTotal);
      }
    });
    
    btnDelete.addActionListener(new MyActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (!preProcess()) {
          return;
        }
        if (process()) {
          display();
        } else {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          return;
        }
        
      }
      
      @Override
      public boolean process() {  
        
        try {
          // Load data below
          ArrayList<ProductItem> listItems = Wrapper.getListItems();
          // Load data above
          
          Salesman.deleteItemFromListItem(listItems, table.getSelectedRow());
          
          // Save data below
          Wrapper.saveListItem(listItems);
          // Save data above
          return true;
        } catch (ClassNotFoundException | IOException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          return false;
        }
  
      }
      
      @Override
      public boolean preProcess() {
        if (table.getSelectedRow() == -1) {
          
          return false;
        }
        return true;
      }
      
      @Override
      public void display() {
        model.removeRow(table.getSelectedRow());
      }
    });
    btnCreate.addActionListener(new MyActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (!preProcess()) {
          return;
        }
        if (process()) {
          JOptionPane.showMessageDialog(null, b.getString("ok"));
          display();
          return;
        }
        JOptionPane.showMessageDialog(null, b.getString("systemError"));
      }
      
      @Override
      public boolean process() {
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Employee> employees = Wrapper.getEmployees();
          ArrayList<Booth> booths = Wrapper.getBooths();
          ArrayList<ExportReceipt> exReceipts = Wrapper.getExportReceipt();
          store.setBooths(booths);
          store.setExportReceipts(exReceipts);
          store.setEmployees(employees);
          
          Salesman currentSalesman = Wrapper.getSalesman();
          ArrayList<ProductItem> listItems = Wrapper.getListItems();
          // Load data above
          Salesman salesman = (Salesman) store.searchEmployee(currentSalesman.getIdEmployee());
          ExportReceipt receipt = salesman.makeExportReceipt(store, listItems, Integer.parseInt(txtTotal.getText()));
       
          // Print receipt
          Wrapper.printExportReceipt(receipt);
          // Save data below
          Wrapper.saveStore(store);
          Wrapper.saveEmployees(store.getEmployees());
          Wrapper.saveBooths(booths);
          Wrapper.saveExportReceipt(exReceipts);
          Wrapper.saveListItem(new ArrayList<>());
          
          // Save data above
          return true;
        } catch (ClassNotFoundException | IOException e) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          return false;
          
        } catch (NoAppriciateResultException e) {
        
          e.printStackTrace();
          return false;
        }
        
      
      }
      
      @Override
      public boolean preProcess() {
        if (model.getRowCount() == 0) {
          JOptionPane.showMessageDialog(null, b.getString("empty"));
          return false;
        }
        return true;
      }
      
      @Override
      public void display() {
        model.setRowCount(0);
        txtTotal.setText("0");
      }
    });
   btnCancel.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();      
        System.exit(0);
      }
    });
   
   btnSearch.addActionListener(new ActionListener() {
     public void actionPerformed(ActionEvent arg0) {
       frame.dispose();
       new SearchProduct();
     }
   });
   
   btnChange.addActionListener(new ActionListener() {
    
    @Override
    public void actionPerformed(ActionEvent e) {
      // TODO Auto-generated method stub
      new ChangePassword1(TypeEmployee.SALESMAN);
    }
  });
  }

}
