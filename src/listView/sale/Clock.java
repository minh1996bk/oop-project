package listView.sale;

import java.util.Date;

import javax.swing.JLabel;

public class Clock extends Thread {
  private JLabel label;
  public Clock(JLabel label) {
    this.label = label;
  }
  
  @Override
  public void run() {
    while(true) {
      this.label.setText(new Date().toString());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

}
