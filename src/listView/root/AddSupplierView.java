package listView.root;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.classes.MediaOne;
import model.classes.Supplier;
import model.wrapper.Wrapper;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.FlowLayout;

public class AddSupplierView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTextField txtName;
  private JTextField txtPhoneNumber;
  private JTextField txtEmail;
  private JTextField txtAddress;


  public AddSupplierView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    this.setVisible(true);
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 450, 465);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    setLocationRelativeTo(null);
    contentPane.setLayout(new GridLayout(12, 1, 0, 0));
    
    JLabel lblNewLabel = new JLabel(b.getString("supplier"));
    contentPane.add(lblNewLabel);
    
    txtName = new JTextField();
    contentPane.add(txtName);
    txtName.setColumns(10);
    
    JLabel lblNewLabel_2 = new JLabel(b.getString("phoneNumber"));
    contentPane.add(lblNewLabel_2);
    
    txtPhoneNumber = new JTextField();
    contentPane.add(txtPhoneNumber);
    txtPhoneNumber.setColumns(10);
    
    JLabel lblNewLabel_3 = new JLabel(b.getString("email"));
    contentPane.add(lblNewLabel_3);
    
    txtEmail = new JTextField();
    contentPane.add(txtEmail);
    txtEmail.setColumns(10);
    
    JLabel lblNewLabel_4 = new JLabel(b.getString("address"));
    contentPane.add(lblNewLabel_4);
    
    txtAddress = new JTextField();
    contentPane.add(txtAddress);
    txtAddress.setColumns(10);
    
    JPanel panel = new JPanel();
    contentPane.add(panel);
    panel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));
    
    JButton btnSave = new JButton(b.getString("save"));
    btnSave.addActionListener(new MyActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (!preProcess()) {
          return;
        }
        if (process()) {
          JOptionPane.showMessageDialog(null, b.getString("ok"));
          display();
          return;
        }
        JOptionPane.showMessageDialog(null, b.getString("systemError"));
      }
      
      @Override
      public boolean process() {
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          // Load data above
          
          Supplier supplier = new Supplier();
          supplier.setSupplierName(txtName.getText());
          supplier.setPhoneNumber(txtPhoneNumber.getText());
          supplier.setEmail(txtEmail.getText());
          supplier.setAddress(txtAddress.getText());
          store.addNewSupplier(supplier);
          
          // Save data below
          Wrapper.saveStore(store);
          // Save data above
          
          return true;
        } catch (ClassNotFoundException | IOException e) {
          e.printStackTrace();
          return false;
        }
      }
      
      @Override
      public boolean preProcess() {
        if (txtName.getText().equals("")
        
            || txtEmail.getText().equals("")
            || txtPhoneNumber.getText().equals("")
            || txtAddress.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("fillAlls"));
          return false;
        }
        return true;
      }
      
      @Override
      public void display() {
        txtName.setText(null);
    
        txtEmail.setText(null);
        txtPhoneNumber.setText(null);
        txtAddress.setText(null);
        
      }
    });
    panel.add(btnSave);
    
    JButton btnCancel = new JButton(b.getString("back"));
    btnCancel.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
      }
    });
    panel.add(btnCancel);
  }

}
