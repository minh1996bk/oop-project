package listView.root;

import java.awt.event.ActionListener;

public interface MyActionListener extends ActionListener {
  public boolean preProcess();
  public boolean process();
  public void display();
}
