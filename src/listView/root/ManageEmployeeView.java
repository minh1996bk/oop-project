package listView.root;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import listView.sale.Clock;
import model.classes.Employee;
import model.classes.MediaOne;
import model.classes.Salesman;
import model.wrapper.NoAppriciateResultException;
import model.wrapper.Wrapper;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ManageEmployeeView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTextField txtDataInput;
  private JTable table;
  private MyModel model;

 
  public ManageEmployeeView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    this.setVisible(true);
    setTitle(b.getString("manage") + " " + b.getString("employee"));
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 688, 477);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    setLocationRelativeTo(null);
    contentPane.setLayout(new BorderLayout(0, 0));
    
    JPanel panel = new JPanel();
    contentPane.add(panel, BorderLayout.NORTH);
    
    JLabel lblNewLabel = new JLabel(b.getString("search"));
    panel.add(lblNewLabel);
    
    txtDataInput = new JTextField();
    panel.add(txtDataInput);
    txtDataInput.setColumns(10);
    
    JButton btnFilter = new JButton(b.getString("find"));

    panel.add(btnFilter);
    
    JPanel clockPanel = new JPanel();
    contentPane.add(clockPanel, BorderLayout.SOUTH);
    
    JLabel lblMyClock = new JLabel();
    Clock clock = new Clock(lblMyClock);
    clock.start();
    clockPanel.add(lblMyClock);
    
    JPanel btnPanel = new JPanel();
    contentPane.add(btnPanel, BorderLayout.WEST);
    btnPanel.setLayout(new GridLayout(10, 2, 10, 10));

    JButton btnAdd = new JButton(b.getString("add") + " " + b.getString("employee"));

    btnPanel.add(btnAdd);
    
    
    JButton btnPaySalary = new JButton(b.getString("pay") + " " + b.getString("salary"));

    btnPanel.add(btnPaySalary);
    
    JButton btnBack = new JButton(b.getString("back"));

    btnPanel.add(btnBack);

    
    
    
    JScrollPane scrollPane = new JScrollPane();
    contentPane.add(scrollPane, BorderLayout.CENTER);
    scrollPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    table = new JTable();
    
    String[] columnNames = {b.getString("id"), b.getString("name"), b.getString("position")};
    model = new MyModel(columnNames);
    
    try {
      // Load data below
      ArrayList<Employee> employees = Wrapper.getEmployees();
      // Load data above
      
      for (Employee employee : employees) {
        String[] row = {"" + employee.getIdEmployee(), employee.getEmployeeName(), employee.getPosition()};
        model.addRow(row);
      }
    } catch (ClassNotFoundException | IOException e) {
      e.printStackTrace();
    }
    table.setModel(model);

    scrollPane.setViewportView(table);
    
    // handle events below
    //
    //
    //
    btnFilter.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (txtDataInput.getText().equals("")) {
          return;
        }
        MediaOne store;
        ArrayList<Employee> employees;
        try {
          // Load data below
          store = Wrapper.getStore();
          employees = Wrapper.getEmployees();
          store.setEmployees(employees);
          // Load data above
        } catch (ClassNotFoundException | IOException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e1.printStackTrace();
          return;
        }
        
        String[] columnNames = {b.getString("id"), b.getString("name"), b.getString("position")};
        model = new MyModel(columnNames);
        try {
          int employeeId = Integer.parseInt(txtDataInput.getText());
          try {
            Employee employee = store.searchEmployee(employeeId);
            String[] row = {"" + employee.getIdEmployee(), employee.getEmployeeName(), employee.getPosition()};
            model.addRow(row);
            table.setModel(model);
          } catch (NoAppriciateResultException e1) {
            String[] row = {b.getString("nodata")};
            model.addRow(row);
            table.setModel(model);
          }
        } catch (NumberFormatException ex) {
          String name = txtDataInput.getText();
          try {
            ArrayList<Employee> results = store.searchEmployee(name);
            for (Employee employee : results) {
              String[] row = {"" + employee.getIdEmployee(), employee.getEmployeeName(), employee.getPosition()};
              model.addRow(row);
              table.setModel(model);
            }
          } catch (NoAppriciateResultException e1) {
            String[] row = {b.getString("nodata")};
            model.addRow(row);
            table.setModel(model);
          }
        }
      }
    });
    
    
    table.addMouseListener(new MouseListener() {
      
      @Override
      public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseEntered(MouseEvent e) {
        
      }
      
      @Override
      public void mouseClicked(MouseEvent arg0) {
        if (arg0.getClickCount() != 2) {
          return;
        }
        int rowIndex = table.getSelectedRow();
        String stringId = (String) model.getValueAt(rowIndex, 0);
        int employeeId = Integer.parseInt(stringId);
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Employee> employees = Wrapper.getEmployees();
          store.setEmployees(employees);
          // Load data above
          
          Employee employee = store.searchEmployee(employeeId);
          EmployeeInfoView view = new EmployeeInfoView();
          view.getTxtId().setText("" + employee.getIdEmployee());
          DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
          view.getTxtName().setText(employee.getEmployeeName());
          view.getTxtBirthday().setText(df.format(employee.getBirthDay()));
          view.getTxtSex().setText(employee.getSex().toString());
          view.getTxtIdentification().setText(employee.getIdentification());
          view.getTxtPhoneNumber().setText(employee.getPhoneNumber());
          view.getTxtMail().setText(employee.getEmail());
          view.getTxtAddress().setText(employee.getAddress());
          view.getTxtNativeLand().setText(employee.getNativeLand());
          view.getLblImage().setIcon(new ImageIcon(employee.getImage()));
          view.getTxtCoefficientSalary().setText("" + employee.getCoefficientsSalary());
          if (employee instanceof Salesman) {
            Salesman salesman = (Salesman) employee;
            view.getTxtCommission().setText("" + salesman.getCommission());
            view.getTxtCommission().setVisible(true);
            view.getLblCommission().setVisible(true);
          }
        } catch (ClassNotFoundException | IOException e) {
          e.printStackTrace();
        } catch (NoAppriciateResultException e) {
          e.printStackTrace();
        }
      }
    });
    
    btnAdd.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        new AddEmployeeView();        
        frame.dispose();
      }
    });
    
    btnPaySalary.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
     
        new SalaryTableView();
        
      }
    });
    
    btnBack.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
        new TabView();
        
      }
    });
  }

}
