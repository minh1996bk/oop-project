package listView.root;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.classes.Cost;
import model.classes.Employee;
import model.classes.MediaOne;
import model.classes.OtherCost;
import model.wrapper.Wrapper;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.GridLayout;

public class SalaryTableView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private MediaOne store;
  private JTable table;
  private JTextField txtDate;
  private JTextField txtTotal;
  
  public SalaryTableView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    setVisible(true);
    JFrame frame = this;
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 450, 300);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(new BorderLayout(0, 0));
    
    try {
      store = Wrapper.getStore();
      ArrayList<Employee> employees = Wrapper.getEmployees();
      store.setEmployees(employees);
    } catch (ClassNotFoundException | IOException e) {
      JOptionPane.showMessageDialog(null, b.getString("systemError"));
      e.printStackTrace();
    }
    
    JPanel panel = new JPanel();
    contentPane.add(panel, BorderLayout.NORTH);
    panel.setLayout(new GridLayout(5, 2, 10, 10));
    
    JLabel lblDate = new JLabel(b.getString("date") + " : ");
    panel.add(lblDate);
    
    txtDate = new JTextField();
    panel.add(txtDate);
    txtDate.setText(new Date().toString());
    txtDate.setEditable(false);
    txtDate.setColumns(10);
    
    JLabel lblTotal = new JLabel(b.getString("total") + " : ");
    panel.add(lblTotal);
    
    txtTotal = new JTextField();
    panel.add(txtTotal);
    txtTotal.setEditable(false);
    txtTotal.setText("" + store.countSalary());
    txtTotal.setColumns(10);
    
    JScrollPane scrollPane = new JScrollPane();
    contentPane.add(scrollPane, BorderLayout.CENTER);
    
    table = new JTable();
    scrollPane.setViewportView(table);
    String[] columnNames = {b.getString("id"), b.getString("name"), b.getString("position"), b.getString("commission"), b.getString("salary")};
    MyModel model = new MyModel(columnNames);
    for (Employee employee : store.getEmployees()) {
      String[] row = {"" + employee.getIdEmployee(), employee.getEmployeeName(), employee.getPosition(),
          "" + employee.getCommission(), "" + employee.countSalary()};
      model.addRow(row);
    }
    table.setModel(model);
    
    JPanel panel_1 = new JPanel();
    contentPane.add(panel_1, BorderLayout.SOUTH);
    
    JButton btnPrint = new JButton(b.getString("print"));
    btnPrint.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
        Cost costName = new Cost("Pay salary");
        OtherCost cost = new OtherCost(costName, store.countSalary());
        store.payOtherCost(cost);
        try {
          Wrapper.saveStore(store);
          Wrapper.printSalaryTable(store.getEmployees(), "/home/leo/Documents/data.txt");
          JOptionPane.showMessageDialog(null, b.getString("ok"));
        } catch (IOException e1) {
          e1.printStackTrace();
        }
 
      }
    });
    panel_1.add(btnPrint);
    
    
  }

}
