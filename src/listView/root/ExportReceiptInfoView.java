package listView.root;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.classes.Book;
import model.classes.Film;
import model.classes.MediaOne;
import model.classes.MusicAlbum;
import model.classes.Product;
import model.wrapper.NoAppriciateResultException;
import model.wrapper.Wrapper;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class ExportReceiptInfoView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTable table;
  private JLabel lblTotal;
  private JLabel lblSalesmanId;
  private JLabel lblDate;
  private JLabel lblReceiptID;


  public ExportReceiptInfoView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    setVisible(true);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 599, 410);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(new BorderLayout(0, 0));
    
    JPanel panel = new JPanel();
    contentPane.add(panel, BorderLayout.NORTH);
    panel.setLayout(new GridLayout(6, 2, 0, 0));
    
    JLabel lblReceipt = new JLabel(b.getString("exportReceiptID"));
    panel.add(lblReceipt);
    
    lblReceiptID = new JLabel("");
    panel.add(lblReceiptID);
    
    JLabel lblDateLabel = new JLabel(b.getString("date"));
    panel.add(lblDateLabel);
    
    lblDate = new JLabel("");
    panel.add(lblDate);
    
    JLabel lblSalesmanid = new JLabel(b.getString("salesmanID"));
    panel.add(lblSalesmanid);
    
    lblSalesmanId = new JLabel("");
    panel.add(lblSalesmanId);
    
    JLabel lblTo = new JLabel(b.getString("total"));
    panel.add(lblTo);
    
    lblTotal = new JLabel("");
    panel.add(lblTotal);
    
    JButton btnBack = new JButton(b.getString("back"));
    btnBack.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
      }
    });
    panel.add(btnBack);
    JScrollPane scrollPane = new JScrollPane();
    contentPane.add(scrollPane, BorderLayout.CENTER);
    
    table = new JTable();
    MyModel model = new MyModel(new String[] {b.getString("back")} );
    table.setModel(model);
    scrollPane.setViewportView(table);
    
    table.addMouseListener(new MouseListener() {
      
      @Override
      public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
        if (e.getClickCount() != 2) {
          return;
        }
        int rowIndex = table.getSelectedRow();
      
        
        String stringId = (String) table.getModel().getValueAt(rowIndex, 0);
      
        int productId = Integer.parseInt(stringId);
        
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Product> products = Wrapper.getProducts();
          // Load data above
          
          store.setProducts(products);
          Product product = store.searchProductById(productId);
          ProductInfoView view = new ProductInfoView();
          if (product instanceof MusicAlbum) {
            MusicAlbum music = (MusicAlbum) product;
            view.getTxtId().setText("" + music.getIdProduct());
            view.getTxtName().setText(music.getProductName());
            view.getTxtPrice().setText("" + music.getPrice());
            view.getLblOp1().setText(b.getString("musicGenre"));
            view.getTxtOp1().setText(music.getMusicGenre().getName());
            view.getLblOp2().setText(b.getString("singer"));
            view.getTxtOp2().setText(music.getSinger());
            view.getLblOp3().setText(b.getString("composer"));
            view.getTxtOp3().setText(music.getComposer());
          } else if (product instanceof Book) {
            Book book = (Book) product;
            view.getTxtId().setText("" + book.getIdProduct());
            view.getTxtName().setText(book.getProductName());
            view.getTxtPrice().setText("" + book.getPrice());
            view.getLblOp1().setText(b.getString("bookGenre"));
            view.getTxtOp1().setText(book.getBookGenre().getName());
            view.getLblOp2().setText(b.getString("content"));
            view.getTxtOp2().setText(book.getSummaryContents());
            view.getLblOp3().setText(b.getString("author"));
            view.getTxtOp3().setText(book.getAuthor());
          } else if (product instanceof Film) {
            Film film = (Film) product;
            view.getTxtId().setText("" + film.getIdProduct());
            view.getTxtName().setText(film.getProductName());
            view.getTxtPrice().setText("" + film.getPrice());
            view.getLblOp1().setText(b.getString("filmGenre"));
            view.getTxtOp1().setText(film.getFilmGenre().getName());
            view.getLblOp2().setText(b.getString("actors"));
            view.getTxtOp2().setText(film.getActorNames());
            view.getLblOp3().setText(b.getString("content"));
            view.getTxtOp3().setText(film.getSummaryContents());
          }
        } catch (ClassNotFoundException | IOException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e1.printStackTrace();
        } catch (NoAppriciateResultException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e1.printStackTrace();
        }
        
      }
    });
  }


  public JTable getTable() {
    return table;
  }


  public void setTable(JTable table) {
    this.table = table;
  }


  public JLabel getLblTotal() {
    return lblTotal;
  }


  public void setLblTotal(JLabel lblTotal) {
    this.lblTotal = lblTotal;
  }


  public JLabel getLblSalesmanId() {
    return lblSalesmanId;
  }


  public void setLblSalesmanId(JLabel lblSalesmanId) {
    this.lblSalesmanId = lblSalesmanId;
  }


  public JLabel getLblDate() {
    return lblDate;
  }


  public void setLblDate(JLabel lblDate) {
    this.lblDate = lblDate;
  }


  public JLabel getLblReceiptID() {
    return lblReceiptID;
  }


  public void setLblReceiptID(JLabel lblReceiptID) {
    this.lblReceiptID = lblReceiptID;
  }
  
  
}
