package listView.root;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.classes.Employee;
import model.classes.MediaOne;
import model.classes.Salesman;
import model.wrapper.NoAppriciateResultException;
import model.wrapper.Wrapper;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.awt.event.ActionEvent;

public class EmployeeInfoView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTextField txtId;
  private JTextField txtName;
  private JTextField txtBirthday;
  private JTextField txtSex;
  private JTextField txtIdentification;
  private JTextField txtPhoneNumber;
  private JTextField txtMail;
  private JTextField txtNativeLand;
  private JTextField txtAddress;
  private JLabel lblImage;
  private JTextField txtCoefficientSalary;
  private JTextField txtCommission;
  private JLabel lblCommission;

  public EmployeeInfoView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    setVisible(true);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 625, 532);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    
    lblImage = new JLabel(b.getString("image"));
    lblImage.setBounds(404, 70, 181, 208);
    
    JLabel lblId = new JLabel(b.getString("id"));
    lblId.setBounds(30, 70, 150, 20);
    
    txtId = new JTextField();
    txtId.setBounds(185, 70, 200, 20);
    txtId.setColumns(10);
    txtId.setEditable(false);
    
    JLabel lblNewLabel = new JLabel(b.getString("name"));
    lblNewLabel.setBounds(30, 100, 150, 20);
    
    txtName = new JTextField();
    txtName.setBounds(185, 100 , 200, 20);
    txtName.setColumns(10);
    
    JLabel lblBirthday = new JLabel(b.getString("birthday"));
    lblBirthday.setBounds(30, 130, 150, 20);
    
    txtBirthday = new JTextField();
    txtBirthday.setBounds(185, 130, 200, 20);
    txtBirthday.setColumns(10);
    
    JLabel lblSex = new JLabel(b.getString("sex"));
    lblSex.setBounds(30, 160, 150, 20);
    
    txtSex = new JTextField();
    txtSex.setBounds(185, 160, 200, 20);
    txtSex.setColumns(10);
    
    JLabel lblIdentification = new JLabel(b.getString("identification"));
    lblIdentification.setBounds(30, 190, 150, 20);
    
    txtIdentification = new JTextField();
    txtIdentification.setBounds(185, 190, 200, 20);
    txtIdentification.setColumns(10);
    
    JLabel lblPhoneContact = new JLabel(b.getString("phoneNumber"));
    lblPhoneContact.setBounds(30, 220, 150, 20);
    
    txtPhoneNumber = new JTextField();
    txtPhoneNumber.setBounds(185, 220, 200, 20);
    txtPhoneNumber.setColumns(10);
    
    JLabel lblMailContact = new JLabel(b.getString("email"));
    lblMailContact.setBounds(30, 250, 150, 20);
    
    txtMail = new JTextField();
    txtMail.setBounds(185, 250, 200, 20);
    txtMail.setColumns(10);
    
    JLabel lblNativeLand = new JLabel(b.getString("nativeLand"));
    lblNativeLand.setBounds(30, 280, 150, 20);
    
    txtNativeLand = new JTextField();
    txtNativeLand.setBounds(185, 280, 400, 20);
    txtNativeLand.setColumns(10);
    
    JLabel lblAddress = new JLabel(b.getString("address"));
    lblAddress.setBounds(30, 310, 150, 20);
    
    txtAddress = new JTextField();
    txtAddress.setBounds(185, 310, 400, 20);
    txtAddress.setColumns(10);
    
    JButton btnSave = new JButton(b.getString("save"));
    btnSave.setBounds(30, 413, 112, 25);

    JButton btnBack = new JButton(b.getString("back"));
    btnBack.setBounds(185, 413, 112, 25);

    
    JLabel lblCoefficientSalary = new JLabel(b.getString("coefficient")+ " " + b.getString("salary"));
    lblCoefficientSalary.setBounds(30, 340, 150, 20);
    
    txtCoefficientSalary = new JTextField();
    txtCoefficientSalary.setBounds(185, 340, 200, 20);
    txtCoefficientSalary.setColumns(10);
    
    lblCommission = new JLabel(b.getString("commission"));
    lblCommission.setBounds(30, 370, 150, 20);
    lblCommission.setVisible(false);
    
    txtCommission = new JTextField();
    txtCommission.setBounds(185, 370, 200, 20);
    txtCommission.setVisible(false);
    txtCommission.setColumns(10);
    contentPane.setLayout(null);
    contentPane.add(lblPhoneContact);
    contentPane.add(lblMailContact);
    contentPane.add(txtNativeLand);
    contentPane.add(txtAddress);
    contentPane.add(txtMail);
    contentPane.add(txtPhoneNumber);
    contentPane.add(lblImage);
    contentPane.add(lblNativeLand);
    contentPane.add(lblAddress);
    contentPane.add(lblIdentification);
    contentPane.add(txtIdentification);
    contentPane.add(lblSex);
    contentPane.add(txtSex);
    contentPane.add(lblId);
    contentPane.add(txtId);
    contentPane.add(lblNewLabel);
    contentPane.add(txtName);
    contentPane.add(lblBirthday);
    contentPane.add(txtBirthday);
    contentPane.add(lblCoefficientSalary);
    contentPane.add(txtCoefficientSalary);
    contentPane.add(lblCommission);
    contentPane.add(txtCommission);
    contentPane.add(btnSave);
    contentPane.add(btnBack);
    
    // handle event below
    //
    //
    // handle save event
    btnSave.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Employee> employees = Wrapper.getEmployees();
          store.setEmployees(employees);
          // Load data above
          
          Employee employee = store.searchEmployee(Integer.parseInt(txtId.getText()));
          if (employee instanceof Salesman) {
            Salesman salesman = (Salesman) employee;
            salesman.setEmployeeName(txtName.getText());
            salesman.setAddress(txtAddress.getText());
            salesman.setBirthDay(df.parse(txtBirthday.getText()));
            salesman.setEmail(txtMail.getText());
            salesman.setNativeLand(txtNativeLand.getText());
            salesman.setCoefficientsSalary(Integer.parseInt(txtCoefficientSalary.getText()));
            salesman.setPhoneNumber(txtPhoneNumber.getText());
            salesman.setCommission(Integer.parseInt(txtCommission.getText()));
            salesman.setIdentification(txtIdentification.getText());
            
          } else {
            employee.setEmployeeName(txtName.getText());
            employee.setAddress(txtAddress.getText());
            employee.setBirthDay(df.parse(txtBirthday.getText()));
            employee.setEmail(txtMail.getText());
            employee.setNativeLand(txtNativeLand.getText());
            employee.setCoefficientsSalary(Integer.parseInt(txtCoefficientSalary.getText()));
            employee.setPhoneNumber(txtPhoneNumber.getText());
            employee.setIdentification(txtIdentification.getText());
          }
          
          // Save data below
          Wrapper.saveEmployees(employees);
         
          // Save data above
          JOptionPane.showMessageDialog(null, b.getString("ok"));
          frame.dispose();
        } catch (ClassNotFoundException | IOException | NumberFormatException | NoAppriciateResultException e) {
          e.printStackTrace();
          return;
        } catch (ParseException e) {
          JOptionPane.showMessageDialog(null, b.getString("date") + " " + b.getString("inWrongFormat"));
          e.printStackTrace();
          return;
        }
      }
    });
    
    // handle back event
    btnBack.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        frame.dispose();
      }
    });
    
  }

  public JLabel getLblCommission() {
    return lblCommission;
  }

  public void setLblCommission(JLabel lblCommission) {
    this.lblCommission = lblCommission;
  }

  public JTextField getTxtId() {
    return txtId;
  }

  public void setTxtId(JTextField txtId) {
    this.txtId = txtId;
  }

  public JTextField getTxtName() {
    return txtName;
  }

  public void setTxtName(JTextField txtName) {
    this.txtName = txtName;
  }

  public JTextField getTxtBirthday() {
    return txtBirthday;
  }

  public void setTxtBirthday(JTextField txtBirthday) {
    this.txtBirthday = txtBirthday;
  }

  public JTextField getTxtSex() {
    return txtSex;
  }

  public void setTxtSex(JTextField txtSex) {
    this.txtSex = txtSex;
  }

  public JTextField getTxtIdentification() {
    return txtIdentification;
  }

  public void setTxtIdentification(JTextField txtIdentification) {
    this.txtIdentification = txtIdentification;
  }

  public JTextField getTxtPhoneNumber() {
    return txtPhoneNumber;
  }

  public void setTxtPhoneNumber(JTextField txtPhoneNumber) {
    this.txtPhoneNumber = txtPhoneNumber;
  }

  public JTextField getTxtMail() {
    return txtMail;
  }

  public void setTxtMail(JTextField txtMail) {
    this.txtMail = txtMail;
  }

  public JTextField getTxtNativeLand() {
    return txtNativeLand;
  }

  public void setTxtNativeLand(JTextField txtNativeLand) {
    this.txtNativeLand = txtNativeLand;
  }

  public JTextField getTxtAddress() {
    return txtAddress;
  }

  public void setTxtAddress(JTextField txtAddress) {
    this.txtAddress = txtAddress;
  }

  public JLabel getLblImage() {
    return lblImage;
  }

  public void setLblImage(JLabel lblImage) {
    this.lblImage = lblImage;
  }

  public JTextField getTxtCoefficientSalary() {
    return txtCoefficientSalary;
  }

  public void setTxtCoefficientSalary(JTextField txtCoefficientSalary) {
    this.txtCoefficientSalary = txtCoefficientSalary;
  }

  public JTextField getTxtCommission() {
    return txtCommission;
  }

  public void setTxtCommission(JTextField txtCommission) {
    this.txtCommission = txtCommission;
  }
  
  
}
