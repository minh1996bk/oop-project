package listView.root;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.classes.Product;
import model.classes.Book;
import model.classes.Booth;
import model.classes.Employee;
import model.classes.ExportReceipt;
import model.classes.Film;
import model.classes.ImportReceipt;
import model.classes.MediaOne;
import model.classes.MusicAlbum;
import model.classes.ProductItem;
import model.wrapper.InvalidExportReceiptIdException;
import model.wrapper.NoAppriciateResultException;
import model.wrapper.Wrapper;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.JTextField;

public class ManageProductView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTable table;
  private JTextField txtInput;
  private JComboBox<String> cmbType;
  private JComboBox<Integer> cmbAmount;
  private JComboBox<String> cmbOption;
  private MyModel model;


  public ManageProductView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    setVisible(true);
    setTitle(b.getString("manage") + " " + b.getString("product"));
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 604, 300);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    setLocationRelativeTo(null);
    contentPane.setLayout(new BorderLayout(0, 0));
    
    JPanel panel = new JPanel();
    FlowLayout flowLayout = (FlowLayout) panel.getLayout();
    flowLayout.setHgap(10);
    contentPane.add(panel, BorderLayout.NORTH);
    
    JLabel lblType = new JLabel(b.getString("type"));
    panel.add(lblType);
    
    cmbType = new JComboBox<>();
    cmbType.setModel(new DefaultComboBoxModel<>(new String[] {b.getString("musicCD"), b.getString("filmCD"), b.getString("book")}));
    panel.add(cmbType);
    
    JLabel lblNumberRecord = new JLabel(b.getString("number") + " " + b.getString("record"));
    panel.add(lblNumberRecord);
    
    cmbAmount = new JComboBox<>();
    cmbAmount.setModel(new DefaultComboBoxModel<>(new Integer[] {10, 50, 100}));
    cmbAmount.setEditable(true);
    panel.add(cmbAmount);
    
    JButton btnFilter = new JButton(b.getString("find"));
    
    panel.add(btnFilter);
    
    JScrollPane scrollPane = new JScrollPane();
    contentPane.add(scrollPane, BorderLayout.CENTER);
    
    table = new JTable();
    String[] columnNames = {b.getString("id") , b.getString("name"), b.getString("number"), b.getString("booth")};
    model = new MyModel(columnNames);
    table.setModel(model);
    scrollPane.setViewportView(table);
    
    JPanel panel_1 = new JPanel();
    contentPane.add(panel_1, BorderLayout.SOUTH);
    panel_1.setLayout(new GridLayout(3, 2, 20, 10));
    
    JLabel lblTimKiemTheo = new JLabel(b.getString("search"));
    panel_1.add(lblTimKiemTheo);
    
    cmbOption = new JComboBox<>();
    cmbOption.setModel(new DefaultComboBoxModel<>(new String[] {b.getString("productId"), b.getString("importReceiptID"), b.getString("exportReceiptID")}));
    panel_1.add(cmbOption);
    
    txtInput = new JTextField();
    panel_1.add(txtInput);
    txtInput.setColumns(10);
    
    JButton btnFind = new JButton(b.getString("find"));

    panel_1.add(btnFind);
    
    JButton btnBack = new JButton(b.getString("back"));
   
    panel_1.add(btnBack);
    
    // handle event below
    //
    //
    //
    //
    //
    // handle back event
    btnBack.addActionListener(new ActionListener() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          frame.dispose();
          new TabView();
        }
      });
    
    // handle filter result event
    
    btnFilter.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        try {
          // Load data
          MediaOne store = Wrapper.getStore();
          ArrayList<Booth> booths = Wrapper.getBooths();
          store.setBooths(booths);
          // Load data
          
          LinkedHashMap<ProductItem, Booth> map = new LinkedHashMap<>();
          int amount;
          try {
            amount = (Integer) cmbAmount.getSelectedItem();
          } catch (ClassCastException e) {
            JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("numericData"));
            return;
          }
          if (amount == 0) {
            
            return;
          }
          int index = 0;
          if (cmbType.getSelectedIndex() == 0) {
            // Filter Music
            for (Booth booth : store.getBooths()) {
              for (ProductItem item : booth.getProductItems()) {
                if (item.getProduct() instanceof MusicAlbum) {
                  map.put(item, booth);
                  index ++;
                }
                if (index >= amount) {
                  break;
                }
              }
              if (index >= amount) {
                break;
              }
            }
          } else if (cmbType.getSelectedIndex() == 1) {
            // Filter Film
            for (Booth booth : store.getBooths()) {
              for (ProductItem item : booth.getProductItems()) {
                if (item.getProduct() instanceof Film) {
                  map.put(item, booth);
                  index ++;
                }
                if (index >= amount) {
                  break;
                }
              }
              if (index >= amount) {
                break;
              }
            }
          } else {
            // Filter Book
            for (Booth booth : store.getBooths()) {
              for (ProductItem item : booth.getProductItems()) {
                if (item.getProduct() instanceof Book) {
                  map.put(item, booth);
                  index ++;
                }
                if (index >= amount) {
                  break;
                }
              }
              if (index >= amount) {
                break;
              }
            }
          }
          ///
          display(map);
        } catch (ClassNotFoundException | IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
        }
      }
      
      // Display result after filter
      
      private void display(LinkedHashMap<ProductItem, Booth> map) {
        String[] columnNames = {b.getString("id") , b.getString("name"), b.getString("number"), b.getString("booth")};
        model = new MyModel(columnNames);
        Product product;
        if (map.isEmpty()) {
          model.addRow(new String[] {b.getString("nodata")});
          table.setModel(model);

          return;
        }
        for (Map.Entry<ProductItem, Booth> pair : map.entrySet()) {
          product = pair.getKey().getProduct();
          String[] row = {"" + product.getIdProduct(), product.getProductName(), "" + pair.getKey().getAmount(), "" + pair.getValue().getName()};
          model.addRow(row);    
        }
          
        table.setModel(model);
      }
    });
    
    // handle find event
    btnFind.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
 
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Employee> employees = Wrapper.getEmployees();
          ArrayList<ExportReceipt> exReceipts = Wrapper.getExportReceipt();
          ArrayList<ImportReceipt> imReceipts = Wrapper.getImportReceipt();
          store.setExportReceipts(exReceipts);
          store.setImportReceipts(imReceipts);
          store.setEmployees(employees);
          // Load data above
          
          int index = cmbOption.getSelectedIndex();
          
          if (index == 0) {
            seekForImportReceiptByProduct(store);
          } else if (index == 1) {
            seekForImportReceiptId(store);
          } else {
            seekForExportReceiptId(store);
          }
          
        } catch (ClassNotFoundException | IOException e) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
        }
      }
      
      private void seekForImportReceiptByProduct(MediaOne store) {
        try {
          int id = Integer.parseInt(txtInput.getText());
          ImportReceipt receipt = store.searchImportReceiptByProduct(id);
          
          ImportReceiptInfoView view = new ImportReceiptInfoView();
          view.getLblReceiptID().setText("" + receipt.getIdReceipt());
          view.getLblTotal().setText("" + receipt.getTotal());
          view.getLblSupplierId().setText("" + receipt.getSupplier().getIdSupplier());
          view.getLblDate().setText(receipt.getTime().toString());
          view.getLblImportId().setText("" + receipt.getImporter().getIdEmployee());
          String[] columns = {b.getString("id"), b.getString("name"), b.getString("number"), b.getString("importPrice")};
          MyModel model = new MyModel(columns);
          for (ProductItem item : receipt.getProductItems()) {
            String[] row = {"" + item.getProduct().getIdProduct(), item.getProduct().getProductName(), 
                "" + item.getAmount(), "" + item.getPrice()};
            model.addRow(row);  
          }
          view.getTable().setModel(model);
          
          
        } catch (NumberFormatException ex) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("numericData"));
          return;
        } catch (NoAppriciateResultException e) {
          JOptionPane.showMessageDialog(null, b.getString("invalidID"));
      
        }
      }
      
      private void seekForExportReceiptId(MediaOne store) {
        int id;
        try {
          id = Integer.parseInt(txtInput.getText());
        } catch (NumberFormatException e) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("numericData"));
          return;
        }
        
        try {
          ExportReceipt receipt = store.searchExportReceipt(id);
          
          ExportReceiptInfoView view = new ExportReceiptInfoView();
          view.getLblReceiptID().setText("" + receipt.getIdReceipt());
          view.getLblDate().setText(receipt.getTime().toString());
          view.getLblSalesmanId().setText("" + receipt.getSalesman().getIdEmployee());
          view.getLblTotal().setText("" + receipt.getTotal());
          
          String[] columns = {b.getString("id"), b.getString("name"), b.getString("number"), b.getString("price")};
          MyModel model = new MyModel(columns);
          for (ProductItem item : receipt.getProductItems()) {
            String[] row = {"" + item.getProduct().getIdProduct(), item.getProduct().getProductName(), 
                "" + item.getAmount(), "" + item.getPrice()};
            model.addRow(row);  
          }
          view.getTable().setModel(model);
        } catch (InvalidExportReceiptIdException | NoAppriciateResultException e) {
          JOptionPane.showMessageDialog(null, b.getString("invalidID"));
        
        } 
      }
      
      private void seekForImportReceiptId(MediaOne store) {
        try {
          int id = Integer.parseInt(txtInput.getText());
          ImportReceipt receipt = store.searchImportReceipt(id);
          
          ImportReceiptInfoView view = new ImportReceiptInfoView();
          view.getLblReceiptID().setText("" + receipt.getIdReceipt());
          view.getLblTotal().setText("" + receipt.getTotal());
          view.getLblSupplierId().setText("" + receipt.getSupplier().getIdSupplier());
          view.getLblDate().setText(receipt.getTime().toString()); 
          view.getLblImportId().setText("" + receipt.getImporter().getIdEmployee());
          String[] columns = {b.getString("id"), b.getString("name"), b.getString("number"), b.getString("importPrice")};
          MyModel model = new MyModel(columns);
          for (ProductItem item : receipt.getProductItems()) {
            String[] row = {"" + item.getProduct().getIdProduct(), item.getProduct().getProductName(), 
                "" + item.getAmount(), "" + item.getPrice()};
            model.addRow(row);  
          }
          view.getTable().setModel(model);
        } catch (NumberFormatException ex) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("numericData"));
          return;
        } catch (NoAppriciateResultException e) {
          JOptionPane.showMessageDialog(null, b.getString("invalidID"));
          
        }
      
      }

    });
    
    // handle double kick row to view info
    
    table.addMouseListener(new MouseListener() {
      
      @Override
      public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        
      }
      
      @Override
      public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
        if (e.getClickCount() != 2) {
          return;
        }
        int rowIndex = table.getSelectedRow();
        String stringId = (String) model.getValueAt(rowIndex, 0);
        int productId;
        try {
          productId  = Integer.parseInt(stringId);
        } catch (NumberFormatException e1) {
         
          return;
        }
        
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Product> products = Wrapper.getProducts();
          store.setProducts(products);
          // Load data above
          
          Product product = store.searchProductById(productId);
          int saleOff = store.getSaleOffPercent(product);
          ProductInfoView view = new ProductInfoView();
          if (product instanceof MusicAlbum) {
            MusicAlbum music = (MusicAlbum) product;
            view.getTxtId().setText("" + music.getIdProduct());
            view.getTxtName().setText(music.getProductName());
            view.getTxtPrice().setText("" + music.getPrice());
            view.getLblOp1().setText(b.getString("musicGenre"));
            view.getTxtOp1().setText(music.getMusicGenre().getName());
            view.getLblOp2().setText(b.getString("singer"));
            view.getTxtOp2().setText(music.getSinger());
            view.getLblOp3().setText(b.getString("composer"));
            view.getTxtOp3().setText(music.getComposer());
            
          } else if (product instanceof Book) {
            Book book = (Book) product;
            view.getTxtId().setText("" + book.getIdProduct());
            view.getTxtName().setText(book.getProductName());
            view.getTxtPrice().setText("" + book.getPrice());
            view.getLblOp1().setText(b.getString("bookGenre"));
            view.getTxtOp1().setText(book.getBookGenre().getName());
            view.getLblOp2().setText(b.getString("content"));
            view.getTxtOp2().setText(book.getSummaryContents());
            view.getLblOp3().setText(b.getString("author"));
            view.getTxtOp3().setText(book.getAuthor());
          } else if (product instanceof Film) {
            Film film = (Film) product;
            view.getTxtId().setText("" + film.getIdProduct());
            view.getTxtName().setText(film.getProductName());
            view.getTxtPrice().setText("" + film.getPrice());
            view.getLblOp1().setText(b.getString("filmGenre"));
            view.getTxtOp1().setText(film.getFilmGenre().getName());
            view.getLblOp2().setText(b.getString("actors"));
            view.getTxtOp2().setText(film.getActorNames());
            view.getLblOp3().setText(b.getString("content"));
            view.getTxtOp3().setText(film.getSummaryContents());
          }
          view.getTxtSaleOff().setText("" + saleOff);
        } catch (ClassNotFoundException | IOException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e1.printStackTrace();
        } catch (NoAppriciateResultException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e1.printStackTrace();
        }
        
      }
    });
    
  }

}
