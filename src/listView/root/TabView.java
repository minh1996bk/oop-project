package listView.root;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import java.awt.event.ActionEvent;

public class TabView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;


  public TabView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    setTitle(b.getString("managerTab"));
    this.setVisible(true);
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 566, 260);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    this.setLocationRelativeTo(null);
    contentPane.setLayout(null);
    
    JButton btnManageEmployee = new JButton(b.getString("manage") + " " + b.getString("employee"));
    btnManageEmployee.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        frame.dispose();
        new ManageEmployeeView();
      }
    });
    btnManageEmployee.setBounds(26, 72, 222, 25);
    contentPane.add(btnManageEmployee);
    
    JButton btnManageProduct = new JButton(b.getString("manage") + " " + b.getString("product"));
    btnManageProduct.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
        new ManageProductView();
        
      }
    });
    btnManageProduct.setBounds(284, 72, 222, 25);
    contentPane.add(btnManageProduct);
    
    JButton btnSetting = new JButton(b.getString("setting"));
    btnSetting.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
        new SettingView();
      }
    });
    btnSetting.setBounds(26, 125, 222, 25);
    contentPane.add(btnSetting);
    
    JButton btnAnalys = new JButton(b.getString("statistics"));
    btnAnalys.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
        new StatisticsView();
      }
    });
    btnAnalys.setBounds(284, 125, 222, 25);
    contentPane.add(btnAnalys);
   
    
    JButton btnExit = new JButton(b.getString("exit"));
    btnExit.setBounds(284, 175, 222, 25);
    btnExit.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        int index = JOptionPane.showConfirmDialog(null, b.getString("exit") + " ?");
        if (index == 0) {
          frame.dispose();
          System.exit(0);
        } else {
          return;
        }
      }
    });
    contentPane.add(btnExit);
  }

}
