package listView.root;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.classes.MediaOne;
import model.classes.Product;
import model.wrapper.NoAppriciateResultException;
import model.wrapper.Wrapper;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

public class ProductInfoView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTextField txtId;
  private JTextField txtName;
  private JTextField txtPrice;
  private JTextField txtOp1;
  private JTextField txtOp2;
  private JTextField txtOp3;
  private JLabel lblOp1;
  private JLabel lblOp2;
  private JLabel lblOp3;
  private JTextField txtSaleOff;
  private JButton btnSaleOff;
  private JButton btnSave;

  public ProductInfoView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    setVisible(true);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 600, 500);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 10, 5));
    setContentPane(contentPane);
    contentPane.setLayout(new GridLayout(9, 2, 10, 10));
    
    JLabel lblNewLabel = new JLabel(b.getString("id"));
    contentPane.add(lblNewLabel);
    
    txtId = new JTextField();
    contentPane.add(txtId);
    txtId.setEditable(false);
    txtId.setColumns(10);
    
    JLabel lblNewLabel_1 = new JLabel(b.getString("name"));
    contentPane.add(lblNewLabel_1);
    
    txtName = new JTextField();
    contentPane.add(txtName);
    txtName.setColumns(10);
    
    JLabel lblNewLabel_2 = new JLabel(b.getString("price"));
    contentPane.add(lblNewLabel_2);
    
    txtPrice = new JTextField();
    contentPane.add(txtPrice);
    txtPrice.setColumns(10);
    
    lblOp1 = new JLabel();
    contentPane.add(lblOp1);
    
    txtOp1 = new JTextField();
    contentPane.add(txtOp1);
    txtOp1.setColumns(10);
    
    lblOp2 = new JLabel();
    contentPane.add(lblOp2);
    
    txtOp2 = new JTextField();
    contentPane.add(txtOp2);
    txtOp2.setColumns(10);
    
    lblOp3 = new JLabel();
    contentPane.add(lblOp3);
    
    txtOp3 = new JTextField();
    contentPane.add(txtOp3);
    txtOp3.setColumns(10);
    
    btnSaleOff = new JButton(b.getString("saleOff"));
    contentPane.add(btnSaleOff);
    
    txtSaleOff = new JTextField("0");
    txtSaleOff.setColumns(10);
    txtSaleOff.setEditable(false);
    contentPane.add(txtSaleOff);
    
    
    btnSave = new JButton(b.getString("save"));
    contentPane.add(btnSave);
    
    JButton btnBack = new JButton(b.getString("back"));
    contentPane.add(btnBack);
    
    // handle event below
    
    //
    //
    // handle back event
    btnBack.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
      }
    });
    
    btnSave.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          int saleOff;
          try {
            saleOff = Integer.parseInt(txtSaleOff.getText());
            if (saleOff == 0) {
              frame.dispose();
              return;
            }
            if (!txtSaleOff.isEditable()) {
              frame.dispose();
              return;
            }
          } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("numericData"));
            return;
          }
          if (saleOff > 100) {
            JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("numericData") + " <= 100");
            return;
          }
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Product> products = Wrapper.getProducts();
          store.setProducts(products);
          // Load data above
          
          int productId = Integer.parseInt(txtId.getText());
          Product product = store.searchProductById(productId);
          
          
          if (txtSaleOff.isEditable()) {
            store.addProductToSaleOffList(product, saleOff);
            Wrapper.saveStore(store);
            JOptionPane.showMessageDialog(null, b.getString("ok"));
            frame.dispose();
          }
        } catch (ClassNotFoundException | IOException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e1.printStackTrace();
        } catch (NoAppriciateResultException e1) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e1.printStackTrace();
        }
        
        
      }
    });
    btnSaleOff.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        txtSaleOff.setEditable(true);
        
      }
    });
  }

  public JTextField getTxtId() {
    return txtId;
  }

  public void setTxtId(JTextField txtId) {
    this.txtId = txtId;
  }

  public JTextField getTxtName() {
    return txtName;
  }

  public void setTxtName(JTextField txtName) {
    this.txtName = txtName;
  }

  public JTextField getTxtPrice() {
    return txtPrice;
  }

  public void setTxtPrice(JTextField txtPrice) {
    this.txtPrice = txtPrice;
  }

  public JTextField getTxtOp1() {
    return txtOp1;
  }

  public void setTxtOp1(JTextField txtOp1) {
    this.txtOp1 = txtOp1;
  }

  public JTextField getTxtOp2() {
    return txtOp2;
  }

  public void setTxtOp2(JTextField txtOp2) {
    this.txtOp2 = txtOp2;
  }

  public JTextField getTxtOp3() {
    return txtOp3;
  }

  public void setTxtOp3(JTextField txtOp3) {
    this.txtOp3 = txtOp3;
  }

  public JLabel getLblOp1() {
    return lblOp1;
  }

  public void setLblOp1(JLabel lblOp1) {
    this.lblOp1 = lblOp1;
  }

  public JLabel getLblOp2() {
    return lblOp2;
  }

  public void setLblOp2(JLabel lblOp2) {
    this.lblOp2 = lblOp2;
  }

  public JLabel getLblOp3() {
    return lblOp3;
  }

  public void setLblOp3(JLabel lblOp3) {
    this.lblOp3 = lblOp3;
  }

  public JButton getBtnSaleOff() {
    return btnSaleOff;
  }

  public void setBtnSaleOff(JButton btnSaleOff) {
    this.btnSaleOff = btnSaleOff;
  }

  public JButton getBtnSave() {
    return btnSave;
  }

  public void setBtnSave(JButton btnSave) {
    this.btnSave = btnSave;
  }

  public JTextField getTxtSaleOff() {
    return txtSaleOff;
  }

  public void setTxtSaleOff(JTextField txtSaleOff) {
    this.txtSaleOff = txtSaleOff;
  }
  
  
  

}
