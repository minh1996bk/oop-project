package listView.root;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import listView.login.ChangePassword1;
import listView.login.TypeEmployee;
import model.classes.Booth;
import model.classes.Cost;
import model.classes.Employee;
import model.classes.ExportReceipt;
import model.classes.ImportReceipt;
import model.classes.Importer;
import model.classes.MediaOne;
import model.classes.Product;
import model.classes.ProductItem;
import model.classes.Salesman;
import model.wrapper.Wrapper;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;

public class SettingView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTextField txtNewCost;


  public SettingView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    setTitle(b.getString("setting"));
    setVisible(true);
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 685, 412);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    setLocationRelativeTo(null);
    contentPane.setLayout(null);
    
    JLabel lblLanguage = new JLabel(b.getString("language"));
    lblLanguage.setBounds(37, 50, 164, 25);
    contentPane.add(lblLanguage);
    
    JComboBox<String> comboBox = new JComboBox<>();
    comboBox.setModel(new DefaultComboBoxModel<>(new String[] {b.getString("sellect") + " " + b.getString("language"), b.getString("english"), b.getString("vietnamese")}));
    comboBox.setBounds(219, 48, 205, 25);
    contentPane.add(comboBox);
    
    JLabel lblCreateNewBooth = new JLabel(b.getString("create") + " " + b.getString("booth"));
    lblCreateNewBooth.setBounds(37, 100, 159, 25);
    contentPane.add(lblCreateNewBooth);
    
    JButton btnCreate = new JButton(b.getString("create"));
    btnCreate.setBounds(219, 100, 205, 25);
    contentPane.add(btnCreate);
    
    JLabel lblAddNewBookgenre = new JLabel(b.getString("add") + " " + b.getString("bookGenre"));
    lblAddNewBookgenre.setBounds(37, 150, 164, 25);
    contentPane.add(lblAddNewBookgenre);
    
    JLabel lblAddNewMusicgenre = new JLabel(b.getString("add") + " " + b.getString("musicGenre"));
    lblAddNewMusicgenre.setBounds(37, 200, 164, 25);
    contentPane.add(lblAddNewMusicgenre);
    
    JLabel lblAddNewFilmgenre = new JLabel(b.getString("add") + " " + b.getString("filmGenre"));
    lblAddNewFilmgenre.setBounds(37, 250, 164, 25);
    contentPane.add(lblAddNewFilmgenre);
    
    JButton btnBook = new JButton(b.getString("bookGenre"));
    btnBook.setBounds(219, 150, 205, 25);
    contentPane.add(btnBook);
    
    JButton btnMusic = new JButton(b.getString("musicGenre"));

    btnMusic.setBounds(219, 200, 205, 25);
    contentPane.add(btnMusic);
    
    JButton btnFilm = new JButton(b.getString("filmGenre"));
    btnFilm.setBounds(219, 250, 205, 25);

    contentPane.add(btnFilm);
    
    JLabel lblAddNewSupplier = new JLabel(b.getString("add") + " " + b.getString("supplier"));
    lblAddNewSupplier.setBounds(37, 300, 147, 25);
    contentPane.add(lblAddNewSupplier);
    
    JButton btnNewSupplier = new JButton(b.getString("add") + " " + b.getString("supplier"));
   
    btnNewSupplier.setBounds(219, 300, 205, 25);
    contentPane.add(btnNewSupplier);
    
    JButton btnBack = new JButton(b.getString("back"));

    btnBack.setBounds(37, 350, 117, 25);
    contentPane.add(btnBack);
    
    JButton btnReset = new JButton(b.getString("reset"));

    btnReset.setBounds(464, 50, 169, 25);
    contentPane.add(btnReset);
    
    JButton btnNewCost = new JButton(b.getString("costType"));

    btnNewCost.setBounds(464, 100, 169, 25);
    contentPane.add(btnNewCost);
    
    txtNewCost = new JTextField();
    txtNewCost.setBounds(464, 150, 169, 25);
    txtNewCost.setEnabled(false);
    contentPane.add(txtNewCost);
    txtNewCost.setColumns(10);
    
    JButton btnChangePassword = new JButton(b.getString("change") + " " + b.getString("password"));
    
    btnChangePassword.setBounds(464, 200, 169, 25);
    contentPane.add(btnChangePassword);
    setLocationRelativeTo(null);
    
    // handle event below
    
    //
    //
    //
    
    // handle create new booth event
    btnCreate.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {

        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<Booth> booths = Wrapper.getBooths();
          store.setBooths(booths);
          // Load data above
          
          store.addNewBooth();
          
          // Save data below
          Wrapper.saveStore(store);
          Wrapper.saveBooths(booths);
          // Save data above
          
          JOptionPane.showMessageDialog(null, b.getString("ok"));
        } catch (ClassNotFoundException | IOException e) {
          JOptionPane.showMessageDialog(null, "Fail");
          e.printStackTrace();
        }
        
        
      }
    });
    
    // handle create new cost
    btnNewCost.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        if (btnNewCost.getText().equals(b.getString("ok"))) {
          if (txtNewCost.getText().equals("")) {
            return;
          }
          try {
            // Load data below
            MediaOne store = Wrapper.getStore();
            // Load data below
            
            Cost cost = new Cost(txtNewCost.getText());
            store.addNewCost(cost);
            
            // Save data below
            Wrapper.saveStore(store);
            // Save data above
            
            JOptionPane.showMessageDialog(null, b.getString("ok"));
            txtNewCost.setText(null);
            txtNewCost.setEnabled(false);
            btnNewCost.setText(b.getString("costType"));
          } catch (IOException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, b.getString("systemError"));
            e.printStackTrace();
          }
        } else if (btnNewCost.getText().equals(b.getString("costType"))) {
          txtNewCost.setEnabled(true);
          btnNewCost.setText(b.getString("ok"));
        }
      }
    
    });
    
    // handle reset event
    btnReset.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        try {
          MediaOne store = new MediaOne();
          ArrayList<Employee> employees = new ArrayList<>();
          ArrayList<ExportReceipt> exReceipts = new ArrayList<>();
          ArrayList<ImportReceipt> imReceipts = new ArrayList<>();
          ArrayList<Booth> booths = new ArrayList<>();
          ArrayList<Product> products = new ArrayList<>();
          
          Importer importer = new Importer();
          Salesman salesman = new Salesman();
          ArrayList<ProductItem> items = new ArrayList<>();
          // Save data below
          Wrapper.saveStore(store);
          Wrapper.saveEmployees(employees);
          Wrapper.saveExportReceipt(exReceipts);
          Wrapper.saveImportReceipt(imReceipts);
          Wrapper.saveBooths(booths);
          Wrapper.saveProducts(products);
          
          Wrapper.saveImporter(importer);
          
          Wrapper.saveListItem(items);
          Wrapper.saveSalesman(salesman);
          // Save data above
          
          JOptionPane.showMessageDialog(null, b.getString("ok"));
        } catch (IOException e) {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          e.printStackTrace();
        }
      }
    });
    
    // handle back event
    btnBack.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        frame.dispose();
        new TabView();
      }
    });
    
    // handle add new supplier
    btnNewSupplier.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        new AddSupplierView();
      }
    });
    
    // handle add new book genre
    btnBook.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        new NewBookGenreView();        
      }
    });
    
    // handle add new film genre
    btnFilm.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        new NewFilmGenreView();        
      }
    });
    
    // handle add new music genre
    btnMusic.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        new NewMusicGenreView();
      }
    });
    
    // handle change password event
    btnChangePassword.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        
        new ChangePassword1(TypeEmployee.ROOT);
      }
    });
    
    // handle change language event
    comboBox.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        int index = comboBox.getSelectedIndex();
        if (index == 0) {
          return;
        } else {
          int option = JOptionPane.showConfirmDialog(null, b.getString("opRestart"));
          if (option == 0) {
            String language;
            if (index == 1) {
              language = "English";
            } else {
              language = "VietNamese";
            }
            try {
              FileWriter w = new FileWriter("language.txt");
              w.write(language);
              w.close();
              System.exit(0);
              
            } catch (IOException e1) {
              JOptionPane.showMessageDialog(null, b.getString("systemError"));
              e1.printStackTrace();
              return;
            }
            return;
          } else {
            return;
          }
        }
        
        
      }
    });
  }
}
