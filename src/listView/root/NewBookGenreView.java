package listView.root;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.classes.BookGenre;
import model.classes.MediaOne;
import model.wrapper.Wrapper;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;

public class NewBookGenreView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTextField textField;
  
  public NewBookGenreView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    this.setVisible(true);
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 450, 316);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setLocationRelativeTo(null);
    setContentPane(contentPane);
    contentPane.setLayout(null);
    
    JLabel lblName = new JLabel(b.getString("name"));
    lblName.setBounds(26, 40, 70, 15);
    contentPane.add(lblName);
    
    textField = new JTextField();
    textField.setBounds(157, 40, 114, 19);
    contentPane.add(textField);
    textField.setColumns(10);
    
    JLabel lblNewLabel = new JLabel(b.getString("description"));
    lblNewLabel.setBounds(26, 84, 154, 15);
    contentPane.add(lblNewLabel);
    
    JTextArea textArea = new JTextArea();
    textArea.setBounds(26, 123, 390, 103);
    contentPane.add(textArea);
    
    JButton btnSave = new JButton(b.getString("save"));
    btnSave.addActionListener(new MyActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (!preProcess()) {
          return;
        }
        if (process()) {
          JOptionPane.showMessageDialog(null, b.getString("ok"));
          display();
          return;
          
        } else {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
          return;
          
        }
      }
      
      @Override
      public boolean process() {
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          // Load data above
          
          BookGenre genre = new BookGenre();
          genre.setName(textField.getText());
          genre.setDescription(textArea.getText());
          store.addNewBookGenre(genre);
          
          // Save data below
          Wrapper.saveStore(store);
          // Save data above
          
          return true;
        } catch (ClassNotFoundException | IOException e) {
          e.printStackTrace();
          return false;
        }
      }
      
      @Override
      public boolean preProcess() {
        if (textField.getText().equals("")
            || textArea.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("fillAlls"));
          return false;
        }
        return true;
      }
      
      @Override
      public void display() {
        frame.dispose();
      }
    });
    btnSave.setBounds(26, 249, 117, 25);
    contentPane.add(btnSave);
    
    JButton btnCancel = new JButton(b.getString("back"));
    btnCancel.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();        
      }
    });
    btnCancel.setBounds(157, 249, 117, 25);
    contentPane.add(btnCancel);
  }
}
