package listView.root;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.classes.Cost;
import model.classes.Employee;
import model.classes.ExportReceipt;
import model.classes.ImportReceipt;
import model.classes.MediaOne;
import model.classes.OtherCost;
import model.wrapper.Wrapper;

import java.awt.FlowLayout;

public class StatisticsView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTable table;
  private JTextField txtFromDate;
  private JTextField txtToDate;
  private JTextField txtTotal;
  private JComboBox<String> cmbCostType;
  private MyModel model;


  public StatisticsView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    this.setVisible(true);
    setTitle(b.getString("statistics"));
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 682, 377);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setLocationRelativeTo(null);
    contentPane.setLayout(new BorderLayout(0, 0));
    setContentPane(contentPane);
    
    JPanel north = new JPanel();
    contentPane.add(north, BorderLayout.NORTH);
    north.setLayout(new GridLayout(0, 3, 5, 0));
    
    JLabel lblFromDate = new JLabel(b.getString("from"));
    north.add(lblFromDate);
    
    txtFromDate = new JTextField();
    txtFromDate.setText("01/01/2015");
    north.add(txtFromDate);
    txtFromDate.setColumns(10);
    
    JButton btnFromDate = new JButton(b.getString("from") + " " + b.getString("date"));

    north.add(btnFromDate);
    
    JLabel lblToDate = new JLabel(b.getString("to"));
    north.add(lblToDate);
    
    txtToDate = new JTextField();
    txtToDate.setText("01/01/2018");
    north.add(txtToDate);
    txtToDate.setColumns(10);
    
    JButton btnToDate = new JButton(b.getString("to") + " " + b.getString("date"));

    north.add(btnToDate);
    
    JLabel label = new JLabel();
    north.add(label);
    
    JButton btnStatistics = new JButton(b.getString("statistics"));

    north.add(btnStatistics);
    
    JLabel lblNewLabel_3 = new JLabel();
    north.add(lblNewLabel_3);
    
    JPanel west = new JPanel();
    contentPane.add(west, BorderLayout.WEST);
    west.setLayout(new GridLayout(7, 1, 10, 10));
    
    JLabel lblThemChiPhi = new JLabel(b.getString("pay") + " " + b.getString("cost"));
    west.add(lblThemChiPhi);
 
    cmbCostType = new JComboBox<>();
    try {
      MediaOne store = Wrapper.getStore();

      ArrayList<Cost> costs = store.getCosts();
      String[] list = new String[costs.size()];
      for (int i = 0; i < costs.size(); i ++) {
        list[i] = costs.get(i).getName();
      }
      cmbCostType.setModel(new DefaultComboBoxModel<>(list) );
    } catch (ClassNotFoundException | IOException e) {
      JOptionPane.showMessageDialog(null, b.getString("systemError"));
      e.printStackTrace();
    }
    west.add(cmbCostType);
    
    JLabel lblTotal = new JLabel(b.getString("total"));
    west.add(lblTotal);
    
    txtTotal = new JTextField();
    west.add(txtTotal);
    txtTotal.setColumns(10);
    
    JButton btnPay = new JButton(b.getString("pay"));

    west.add(btnPay);
    
    JPanel south = new JPanel();
    FlowLayout flowLayout = (FlowLayout) south.getLayout();
    flowLayout.setAlignment(FlowLayout.RIGHT);
    contentPane.add(south, BorderLayout.SOUTH);
    
    JButton btnBack = new JButton(b.getString("back"));

    south.add(btnBack);
    
    JPanel east = new JPanel();
    contentPane.add(east, BorderLayout.EAST);
    
    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    contentPane.add(scrollPane, BorderLayout.CENTER);
    
    table = new JTable();
    String[] columnNames = {b.getString("name"), b.getString("total"), b.getString("date")};
    model = new MyModel(columnNames);
    table.setModel(model);
    scrollPane.setViewportView(table);
    
    // handle events below
    //
    //
    //
    //
    btnFromDate.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
       
      }
    });
    
    btnStatistics.addActionListener(new MyActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (!preProcess()) {
           return;
        } 
        
        if (process()) {
          display();
          return;
        } else {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
        }
      }
      
      @Override
      public boolean process() {
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          ArrayList<ExportReceipt> exReceipts = Wrapper.getExportReceipt();
          ArrayList<ImportReceipt> imReceipts = Wrapper.getImportReceipt();
          ArrayList<Employee> employees = Wrapper.getEmployees();
          
          store.setEmployees(employees);
          store.setExportReceipts(exReceipts);
          store.setImportReceipts(imReceipts);
          // Load data above
          
          DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
          Date fromDate;
          Date toDate;
          try {
            fromDate = df.parse(txtFromDate.getText());
            toDate = df.parse(txtToDate.getText());
            ArrayList<OtherCost> costs = store.takeStatisticForOtherCost(fromDate, toDate);
            model.setRowCount(0);
            int totalPay = 0;
            for (OtherCost cost : costs) {
              totalPay += cost.getTotalPay();
              String[] row = {cost.getCost().getName(), "" + cost.getTotalPay(), df.format(cost.getTime())};
              model.addRow(row);
            }
            int importTotal = store.countImportTotal(fromDate, toDate);
            int exportTotal = store.countExportTotal(fromDate, toDate);
            int lai = exportTotal - importTotal - totalPay;
            String[] row1 = {b.getString("importTotal"), "" + importTotal, null};
            String[] row2 = {b.getString("revenue"), "" + exportTotal, null};
            String[] row3 = {b.getString("profit"), "" + lai, null};
            model.addRow(row1);
            model.addRow(row2);
            model.addRow(row3);
            return true;
          } catch (ParseException e) {
            JOptionPane.showMessageDialog(null, b.getString("date") + " " + b.getString("inWrongFormat"));
            
          }
          
       
        } catch (ClassNotFoundException | IOException e) {
          e.printStackTrace();
        }
        return false;
      }
      
      @Override
      public boolean preProcess() {
        
        return true;
      }
      
      @Override
      public void display() {
        
      }
    });
    
    btnPay.addActionListener(new MyActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (!preProcess()) {
          return;
        }
        if (process()) {
          JOptionPane.showMessageDialog(null, b.getString("ok"));
          display();
        } else {
          JOptionPane.showMessageDialog(null, b.getString("systemError"));
        }
      }
      
      @Override
      public boolean process() {
        try {
          // Load data below
          MediaOne store = Wrapper.getStore();
          // Load data above
          
          int total = Integer.parseInt(txtTotal.getText());
          String name = (String) cmbCostType.getSelectedItem();
          if (name.equals("")) {
            JOptionPane.showMessageDialog(null, "systemError");
          }
          Cost cost = new Cost(name);
          OtherCost otherCost = new OtherCost(cost, total);
          store.payOtherCost(otherCost);
          
          // Save data below
          Wrapper.saveStore(store);
          // Save data above
          
          return true;
        } catch (ClassNotFoundException | IOException | NullPointerException e) {
          
          return false;
        }
      }
      
      @Override
      public boolean preProcess() {
        if (txtTotal.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("total"));
          return false;
        }
        
        try {
          Integer.parseInt(txtTotal.getText());
          return true;
        } catch (NumberFormatException ex) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("numericData"));
          return false;
        }
        
      }
      
      @Override
      public void display() {
        txtTotal.setText(null);
      }
    });
    
    btnBack.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        frame.dispose();
        new TabView();
      }
    });
    
    btnToDate.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        
      }
    });
  }

}
