package listView.root;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.classes.Account;
import model.classes.Employee;
import model.classes.Importer;
import model.classes.MediaOne;
import model.classes.Salesman;
import model.classes.Sex;
import model.wrapper.Wrapper;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class AddEmployeeView extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JTextField txtName;
  private JTextField txtBirthDay;
  private JTextField txtIdentification;
  private JTextField txtPhoneNuber;
  private JTextField txtEmail;
  private JTextField txtNativeLand;
  private JTextField txtAddress;
  private JLabel lblCommission;
  private JTextField txtCommission;
  private JTextField txtLink;
  private JTextField txtCoefficientsSalary;
  private JLabel lblImage;
  private JComboBox<String> cmbSex;

  public AddEmployeeView() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    JFrame frame = this;
    this.setVisible(true);
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setBounds(100, 100, 800, 700);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    contentPane.setLayout(new BorderLayout(0, 0));
    setLocationRelativeTo(null);
    setContentPane(contentPane);
    
    JPanel west = new JPanel();
    contentPane.add(west, BorderLayout.WEST);
    west.setLayout(new GridLayout(12, 1, 0, 5));
    
    JLabel lblNewLabel = new JLabel(b.getString("type"));
    lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
    lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
    west.add(lblNewLabel);
    
    JComboBox<String> cmbType = new JComboBox<>();
    cmbType.setModel(new DefaultComboBoxModel<>(new String[] {b.getString("employee"), b.getString("salesman"), b.getString("importer")}));

    west.add(cmbType);
    
    JPanel south = new JPanel();
    contentPane.add(south, BorderLayout.SOUTH);
    south.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 5));
    
    JButton btnSave = new JButton(b.getString("save"));

    south.add(btnSave);
    
    JButton btnBack = new JButton(b.getString("back"));

    south.add(btnBack);
    
    JPanel north = new JPanel();
    contentPane.add(north, BorderLayout.NORTH);
    
    JLabel lblEmployeeProfile = new JLabel(b.getString("employeeProfile"));
    north.add(lblEmployeeProfile);
    
    JPanel center = new JPanel();
    contentPane.add(center, BorderLayout.CENTER);
    center.setLayout(new GridLayout(20, 1, 10, 5));
    center.setBorder(new EmptyBorder(20, 20, 10, 10));
    
    JLabel lblNewLabel_1 = new JLabel(b.getString("name"));
    lblNewLabel_1.setVerticalAlignment(SwingConstants.TOP);
    center.add(lblNewLabel_1);
    
    txtName = new JTextField();
    center.add(txtName);
    txtName.setColumns(10);
    
    JLabel lblNewLabel_2 = new JLabel(b.getString("birthday"));
    center.add(lblNewLabel_2);
    
    txtBirthDay = new JTextField();
    center.add(txtBirthDay);
    txtBirthDay.setColumns(10);
    
    JLabel lblNewLabel_3 = new JLabel(b.getString("sex"));
    center.add(lblNewLabel_3);
    
    cmbSex = new JComboBox<>();
    cmbSex.setModel(new DefaultComboBoxModel<>(new String[] {"Nam", "Nu"}));
    center.add(cmbSex);
    
    JLabel lblNewLabel_4 = new JLabel(b.getString("identification"));
    center.add(lblNewLabel_4);
    
    txtIdentification = new JTextField();
    center.add(txtIdentification);
    txtIdentification.setColumns(10);
    
    JLabel lblNewLabel_5 = new JLabel(b.getString("phoneNumber"));
    center.add(lblNewLabel_5);
    
    txtPhoneNuber = new JTextField();
    center.add(txtPhoneNuber);
    txtPhoneNuber.setColumns(10);
    
    JLabel lblNewLabel_6 = new JLabel(b.getString("email"));
    center.add(lblNewLabel_6);
    
    txtEmail = new JTextField();
    center.add(txtEmail);
    txtEmail.setColumns(10);
    
    JLabel lblNewLabel_7 = new JLabel(b.getString("nativeLand"));
    center.add(lblNewLabel_7);
    
    txtNativeLand = new JTextField();
    center.add(txtNativeLand);
    txtNativeLand.setColumns(10);
    
    JLabel lblNewLabel_8 = new JLabel(b.getString("address"));
    center.add(lblNewLabel_8);
    
    txtAddress = new JTextField();
    center.add(txtAddress);
    txtAddress.setColumns(10);
    
    JLabel lblCoefficientsSalary = new JLabel(b.getString("coefficient") + " " + b.getString("salary"));
    center.add(lblCoefficientsSalary);
    
    txtCoefficientsSalary = new JTextField();
    center.add(txtCoefficientsSalary);
    txtCoefficientsSalary.setColumns(10);
    
    lblCommission = new JLabel(b.getString("commission"));
    center.add(lblCommission);
    lblCommission.setVisible(false);
    txtCommission = new JTextField("0");
    center.add(txtCommission);
    txtCommission.setVisible(false);
    
    JPanel east = new JPanel();
    contentPane.add(east, BorderLayout.EAST);
    east.setLayout(new BorderLayout());
    
    lblImage = new JLabel(b.getString("image"));
    east.add(lblImage, BorderLayout.CENTER);
    
    txtLink = new JTextField();
    txtLink.setEditable(false);
    east.add(txtLink, BorderLayout.SOUTH);
    
    JButton btnUpload = new JButton(b.getString("upload"));

    east.add(btnUpload, BorderLayout.NORTH);
    
    // handle event below
    
    //
    //
    //
    //
    //
    // handle change employee type event
    
    cmbType.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent arg0) {
        if (cmbType.getSelectedIndex() == 1) {
          lblCommission.setVisible(true);
          txtCommission.setVisible(true);
        
        } else {
          lblCommission.setVisible(false);
          txtCommission.setVisible(false);
        }
        
      }
    });
    
    // handle choose image event
    btnUpload.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        JFileChooser chooser = new JFileChooser();
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
          String link = chooser.getSelectedFile().getAbsolutePath();
          txtLink.setText(link);
          
          BufferedImage img;
          try {
            lblImage.setText(null);
            img = ImageIO.read(chooser.getSelectedFile());
            lblImage.setIcon(new ImageIcon(img));

          } catch (IOException e) {
            JOptionPane.showMessageDialog(null, b.getString("systemError"));
            e.printStackTrace();
          }
         
          
        } else {
          return;
        }
      }
    });
    
    // handle save new employee event
    btnSave.addActionListener(new MyActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (preProcess()) {
          if(process()) {
            JOptionPane.showMessageDialog(null, b.getString("ok"));
          } else {
            JOptionPane.showMessageDialog(null, b.getString("systemError"));
            
          }
          display();
        }
        
      }
      
      @Override
      public boolean process() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Account account = new Account();
        account.setUserName(txtName.getText());
        account.setPassword(txtName.getText());
        if (cmbType.getSelectedIndex() == 0) {
          try {
            // Load data below
            MediaOne store = Wrapper.getStore();
            ArrayList<Employee> employees = Wrapper.getEmployees();
            store.setEmployees(employees);
            BufferedImage img;
            FileInputStream in = new FileInputStream(txtLink.getText());
            img = ImageIO.read(in);
            // Load data above
            
            Employee employee = new Employee();
            employee.setEmployeeName(txtName.getText());
            employee.setBirthDay(df.parse(txtBirthDay.getText()));
            if (cmbSex.getSelectedIndex() == 0) {
              employee.setSex(Sex.Male);
            } else {
              employee.setSex(Sex.Female);
            }
            employee.setIdentification(txtIdentification.getText());
            employee.setPhoneNumber(txtPhoneNuber.getText());
            employee.setAddress(txtAddress.getText());
            employee.setEmail(txtEmail.getText());
            employee.setCoefficientsSalary(Integer.parseInt(txtCoefficientsSalary.getText()));
            employee.setNativeLand(txtNativeLand.getText()); 
            employee.setImage(img);
            employee.setAccount(account);
            store.addNewEmployee(employee);
            
            // Save data below
            Wrapper.saveStore(store);
            Wrapper.saveEmployees(store.getEmployees());
            // Save data above
            return true;
          } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
            return false;
          } catch (ParseException e) {
            JOptionPane.showMessageDialog(null, b.getString("date") + " " + b.getString("inWrongFormat"));
            e.printStackTrace();
            return false;
          }
        } else if (cmbType.getSelectedIndex() == 1) {
          try {
            // Load data below
            MediaOne store = Wrapper.getStore();
            ArrayList<Employee> employees = Wrapper.getEmployees();
            store.setEmployees(employees);
            BufferedImage img;
            FileInputStream in = new FileInputStream(txtLink.getText());
            img = ImageIO.read(in);
            // Load data above
            
            Salesman salesman = new Salesman();
            salesman.setEmployeeName(txtName.getText());
            
            salesman.setBirthDay(df.parse(txtBirthDay.getText()));
            if (cmbSex.getSelectedIndex() == 0) {
              salesman.setSex(Sex.Male);
            } else {
              salesman.setSex(Sex.Female);
            }
            salesman.setIdentification(txtIdentification.getText());
            salesman.setPhoneNumber(txtPhoneNuber.getText());
            salesman.setAddress(txtAddress.getText());
            salesman.setEmail(txtEmail.getText());
            salesman.setCoefficientsSalary(Integer.parseInt(txtCoefficientsSalary.getText()));
            salesman.setNativeLand(txtNativeLand.getText()); 
            salesman.setImage(img);
            salesman.setCommission(Integer.parseInt(txtCommission.getText()));
            salesman.setAccount(account);
            store.addNewEmployee(salesman);
            
            // Save data below
            Wrapper.saveStore(store);
            Wrapper.saveEmployees(store.getEmployees());
            // Save data above
            
            return true;
          } catch (ClassNotFoundException | IOException e) {
            return false;
          } catch (ParseException e) {
            JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("inWrongFormat"));
            e.printStackTrace();
            return false;
          }        
        } else if (cmbType.getSelectedIndex() == 2) {
          try {
            // Load data below
            MediaOne store = Wrapper.getStore();
            ArrayList<Employee> employees = Wrapper.getEmployees();
            store.setEmployees(employees);
            BufferedImage img;
            FileInputStream in = new FileInputStream(txtLink.getText());
            img = ImageIO.read(in);
            // Load data above
            
            Importer importer = new Importer();
            importer.setEmployeeName(txtName.getText());
            importer.setBirthDay(df.parse(txtBirthDay.getText()));
            if (cmbSex.getSelectedIndex() == 0) {
              importer.setSex(Sex.Male);
            } else {
              importer.setSex(Sex.Female);
            }
            importer.setIdentification(txtIdentification.getText());
            importer.setPhoneNumber(txtPhoneNuber.getText());
            importer.setAddress(txtAddress.getText());
            importer.setEmail(txtEmail.getText());
            importer.setCoefficientsSalary(Integer.parseInt(txtCoefficientsSalary.getText()));
            importer.setNativeLand(txtNativeLand.getText()); 
            importer.setImage(img);
            importer.setAccount(account);
            store.addNewEmployee(importer);
            
            // Save data below
            Wrapper.saveStore(store);
            Wrapper.saveEmployees(store.getEmployees());
            // Save data above
            return true;
          } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
            return false;
          } catch (ParseException e) {
            JOptionPane.showMessageDialog(null, b.getString("date") + " " + b.getString("inWrongFormat"));
            e.printStackTrace();
            return false;
          }
          
        }
        return false;
        
      }
      
      @Override
      public boolean preProcess() {
        if (txtLink.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("upload") + " " + b.getString("image"));
          return false;
        }
        
        if (txtName.getText().equals("")
            || txtBirthDay.getText().equals("")
            || txtIdentification.getText().equals("")
            || txtPhoneNuber.getText().equals("")
            || txtEmail.getText().equals("")
            || txtNativeLand.getText().equals("")
            || txtAddress.getText().equals("")) {
          JOptionPane.showMessageDialog(null, b.getString("fillAlls"));
          return false;
        }
        try {
          Integer.parseInt(txtCoefficientsSalary.getText());
          Integer.parseInt(txtCommission.getText());
        } catch (NumberFormatException ex) {
          JOptionPane.showMessageDialog(null, b.getString("input") + " " + b.getString("numericData"));
          return false;
        }
        
      
        return true;
      }
      
      @Override
      public void display() {
//        txtName.setText(null);
//        txtBirthDay.setText(null);
//        txtAddress.setText(null);
//        txtCoefficientsSalary.setText(null);
//        txtCommission.setText("0");
//        txtLink.setText(null);
//        txtPhoneNuber.setText(null);
//        txtNativeLand.setText(null);
//        txtEmail.setText(null);
//        lblImage.setIcon(null);
//        txtIdentification.setText(null);
//        lblImage.setText(b.getString("image"));
       
      }
    });
    
    // handle back event
    btnBack.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent arg0) {
        new ManageEmployeeView();
        frame.dispose();
        
      }
    });
  }

}
