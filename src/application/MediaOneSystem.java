package application;

import java.awt.EventQueue;
import java.io.IOException;

import listView.login.LoginView;
import model.wrapper.Wrapper;


public class MediaOneSystem {
  /**
   * Launch the application.
   */
  public static void main(String[] args) {
   
    try {
      Wrapper.setLocaleForSystem();
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          LoginView frame = new LoginView();
          frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
}
 