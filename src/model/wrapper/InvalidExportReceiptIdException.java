package model.wrapper;

public class InvalidExportReceiptIdException extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public InvalidExportReceiptIdException() {
    super("Invalid ExportReceiptId");
  }
}
