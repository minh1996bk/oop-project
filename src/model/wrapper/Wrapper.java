package model.wrapper;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.imageio.ImageIO;
import model.classes.Account;
import model.classes.Booth;
import model.classes.Employee;
import model.classes.ExportReceipt;
import model.classes.ImportReceipt;
import model.classes.Importer;
import model.classes.MediaOne;
import model.classes.Product;
import model.classes.ProductItem;
import model.classes.Salesman;

public class Wrapper {
  public static MediaOne getStore() throws IOException, ClassNotFoundException {
    FileInputStream in = new FileInputStream("MediaOne.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    MediaOne mediaOne = (MediaOne) objectIn.readObject();
    objectIn.close();
    return mediaOne;
  }
  
  public static void saveStore(MediaOne mediaOne) throws IOException {
    FileOutputStream out = new FileOutputStream("MediaOne.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(mediaOne);
    objectOut.close();
  }
  
  public static Salesman getSalesman() throws IOException, ClassNotFoundException {
    FileInputStream in = new FileInputStream("Salesman.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    Salesman salesman = (Salesman) objectIn.readObject();
    objectIn.close();
    return salesman;

  }
  
  public static void saveSalesman(Salesman salesman) throws IOException {
    FileOutputStream out = new FileOutputStream("Salesman.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(salesman);
    objectOut.close();
  }
  
  @SuppressWarnings("unchecked")
  public static ArrayList<ProductItem> getListItems() throws ClassNotFoundException, IOException {
    FileInputStream in = new FileInputStream("ListItems.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    ArrayList<ProductItem> listItems = (ArrayList<ProductItem>) objectIn.readObject();
    objectIn.close();
    return listItems;

  }
  
  public static void saveListItem(ArrayList<ProductItem> listItems) throws IOException {
    FileOutputStream out = new FileOutputStream("ListItems.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(listItems);
    objectOut.close();
  }
  
  public static void saveEmployees(ArrayList<Employee> employees) throws IOException {
    FileOutputStream out = new FileOutputStream("Employees.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(employees);
    objectOut.close();
    for (Employee employee : employees) {
      saveImage(employee);
    }
  }
  
  private static void saveImage(Employee employee) throws IOException {
    FileOutputStream out = new FileOutputStream("" + employee.getIdEmployee() + ".jpg");
    ImageIO.write(employee.getImage(), "jpg", out);
  }
  
  public static ArrayList<Employee> getEmployees() throws IOException, ClassNotFoundException {
    FileInputStream in = new FileInputStream("Employees.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    @SuppressWarnings("unchecked")
    ArrayList<Employee> employees = (ArrayList<Employee>) objectIn.readObject();
    objectIn.close();
    
    for (Employee employee : employees) {
      getImage(employee);
    }
    return employees;
    
  }
  
  private static void getImage(Employee employee) throws IOException {
    FileInputStream in = new FileInputStream("" + employee.getIdEmployee() + ".jpg");
    BufferedImage image = ImageIO.read(in);
    employee.setImage(image);
  }
  
  public static void saveExportReceipt(ArrayList<ExportReceipt> receipts) throws IOException {
    FileOutputStream out = new FileOutputStream("ExportReceipt.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(receipts);
    objectOut.close();
  }
  
  public static ArrayList<ExportReceipt> getExportReceipt() throws ClassNotFoundException, IOException {
    FileInputStream in = new FileInputStream("ExportReceipt.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    @SuppressWarnings("unchecked")
    ArrayList<ExportReceipt> receipts = (ArrayList<ExportReceipt>) objectIn.readObject();
    objectIn.close();
    return receipts;
  }
  
  public static void saveImportReceipt(ArrayList<ImportReceipt> receipts) throws IOException {
    FileOutputStream out = new FileOutputStream("ImportReceipt.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(receipts);
    objectOut.close();
  }
  
  public static ArrayList<ImportReceipt> getImportReceipt() throws ClassNotFoundException, IOException {
    FileInputStream in = new FileInputStream("ImportReceipt.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    @SuppressWarnings("unchecked")
    ArrayList<ImportReceipt> receipts = (ArrayList<ImportReceipt>) objectIn.readObject();
    objectIn.close();
    return receipts;
  }
  
  public static void saveBooths(ArrayList<Booth> booths) throws IOException {
    FileOutputStream out = new FileOutputStream("Booth.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(booths);
    objectOut.close();
  }
  
  public static ArrayList<Booth> getBooths() throws IOException, ClassNotFoundException {
    FileInputStream in = new FileInputStream("Booth.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    @SuppressWarnings("unchecked")
    ArrayList<Booth> receipts = (ArrayList<Booth>) objectIn.readObject();
    objectIn.close();
    return receipts;
  }
  
  public static void saveProducts(ArrayList<Product> products) throws IOException {
    FileOutputStream out = new FileOutputStream("Product.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(products);
    objectOut.close();
  }
  
  public static ArrayList<Product> getProducts() throws IOException, ClassNotFoundException {
    FileInputStream in = new FileInputStream("Product.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    @SuppressWarnings("unchecked")
    ArrayList<Product> products = (ArrayList<Product>) objectIn.readObject();
    objectIn.close();
    return products;
  }
  
  
  public static void printSalaryTable(ArrayList<Employee> employees, String url) throws IOException {
    FileWriter w = new FileWriter(url);
    w.write("Salary Table");
    w.write(System.lineSeparator());
    w.write("Date : " + new Date().toString());
    w.write(System.lineSeparator());
    w.write("ID  Name           Position       Commission       Salary");
    w.write(System.lineSeparator());
    for (Employee employee : employees) {
      w.write("" + employee.getIdEmployee());
      w.write(" :  ");
      w.write(employee.getEmployeeName());
      w.write("  : ");
      w.write(employee.getPosition());
      w.write("  : ");
      w.write("" + employee.getCommission());
      w.write("  : ");
      w.write("" + employee.countSalary());
      w.write(System.lineSeparator());
    }
    w.close();
  }
  
  public static void printExportReceipt(ExportReceipt receipt) throws IOException {
    String url = "/home/leo/Documents/export" + receipt.getIdReceipt() + ".txt";
    FileWriter w = new FileWriter(url);
    w.write("Export Receipt Information");
    w.write(System.lineSeparator());
    w.write("Receipt ID : ");
    w.write("" + receipt.getIdReceipt());
    w.write(System.lineSeparator());
    w.write("Date : ");
    w.write(new Date().toString());
    w.write(System.lineSeparator());
    w.write("Detail : ");
    w.write(System.lineSeparator());
    for (ProductItem item : receipt.getProductItems()) {
      w.write("" + item.getProduct().getIdProduct());
      w.write(" : ");
      w.write(item.getProduct().getProductName());
      w.write(" : ");
      w.write("" + item.getAmount());
      w.write(" : ");
      w.write("" + item.getPrice());
      w.write(System.lineSeparator());
    }
    w.write("Total : ");
    w.write("" + receipt.getTotal());
    w.close();
  }
  
  public static void printImportReceipt(ImportReceipt receipt) throws IOException {
    String url = "/home/leo/Documents/import" + receipt.getIdReceipt() + ".txt";
    FileWriter w = new FileWriter(url);
    w.write("Export Receipt Information");
    w.write(System.lineSeparator());
    w.write("Receipt ID : ");
    w.write("" + receipt.getIdReceipt());
    w.write(System.lineSeparator());
    w.write("Date : ");
    w.write(new Date().toString());
    w.write(System.lineSeparator());
    w.write("Detail : ");
    w.write(System.lineSeparator());
    for (ProductItem item : receipt.getProductItems()) {
      w.write("" + item.getProduct().getIdProduct());
      w.write(" : ");
      w.write(item.getProduct().getProductName());
      w.write(" : ");
      w.write("" + item.getAmount());
      w.write(" : ");
      w.write("" + item.getPrice());
      w.write(System.lineSeparator());
    }
    w.write("Total : ");
    w.write("" + receipt.getTotal());
    w.close();
  }
  
  public static void setLocaleForSystem() throws IOException {
    Locale[] locales = {
        new Locale ("en", "US"),
        new Locale ("vi", "VN")
    };
   

    FileReader reader = new FileReader("language.txt");
    BufferedReader br = new BufferedReader(reader);
    
    String language = br.readLine();
    br.close();
    if (language.equals("English")) {
      Locale.setDefault(locales[0]);
      
    } else if (language.equals("VietNamese")) {
      Locale.setDefault(locales[1]);
    } else {
      return;
    }

 
  }
  
  public static void saveAccount(Account account) throws IOException {
    FileOutputStream out = new FileOutputStream("account.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(account);
    objectOut.close();
  }
  
  public static Account getAccount() throws IOException, ClassNotFoundException {
    FileInputStream in = new FileInputStream("account.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    Account account = (Account) objectIn.readObject();
    objectIn.close();
    return account;
  }
  
  public static void saveImporter(Importer importer) throws IOException {
    FileOutputStream out = new FileOutputStream("Importer.txt");
    ObjectOutputStream objectOut = new ObjectOutputStream(out);
    objectOut.writeObject(importer);
    objectOut.close();
  }
  
  public static Importer getImporter() throws IOException, ClassNotFoundException {
    FileInputStream in = new FileInputStream("Importer.txt");
    ObjectInputStream objectIn = new ObjectInputStream(in);
    Importer importer = (Importer) objectIn.readObject();
    objectIn.close();
    return importer;
  }
}
