package model.classes;

import java.io.Serializable;

public enum Sex implements Serializable {
  Male,
  Female
}
