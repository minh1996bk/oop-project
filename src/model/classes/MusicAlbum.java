package model.classes;

public class MusicAlbum extends Product {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String singer;
  private String composer;
  private MusicGenre musicGenre;
  
  public MusicAlbum() {
    
  }

  public String getSinger() {
    return singer;
  }

  public void setSinger(String singer) {
    this.singer = singer;
  }

  public MusicGenre getMusicGenre() {
    return musicGenre;
  }

  public void setMusicGenre(MusicGenre musicGenre) {
    this.musicGenre = musicGenre;
  }

  public String getComposer() {
    return composer;
  }

  public void setComposer(String composer) {
    this.composer = composer;
  }
  
  public boolean checkRequired(String searchPattern, String genre) {
    if (musicGenre.getName().equals(genre)
        && (this.getProductName().contains(searchPattern)
            || this.composer.contains(searchPattern)
            || this.singer.contains(searchPattern)
        )) {
      return true;
    }
    return false;
  }
  
 
  
}
