package model.classes;

import java.io.Serializable;

public class MusicGenre implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String name;
  private String desription;
  
  public MusicGenre() {
    
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return desription;
  }

  public void setDescription(String desription) {
    this.desription = desription;
  }
  
  
  
  
}
