package model.classes;

import java.io.Serializable;
import java.util.Date;

public class OtherCost implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private Cost cost;
  private int totalPay;
  private Date time;
  
  public OtherCost(Cost cost, int totalPay) {
    this.cost = cost;
    this.totalPay = totalPay;
    this.time = new Date();
  }

  public Cost getCost() {
    return cost;
  }

  public void setCost(Cost cost) {
    this.cost = cost;
  }

  public int getTotalPay() {
    return totalPay;
  }

  public void setTotalPay(int totalPay) {
    this.totalPay = totalPay;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }
  
  
}
