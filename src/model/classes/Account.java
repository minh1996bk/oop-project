package model.classes;

import java.io.Serializable;

public class Account implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private String userName;
  private String password;
  
  public Account() {
    
  }
  
  public Account(String userName, String password) {
    this.userName = userName;
    this.password = password;
  }
  
  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
  



  public boolean equals(Account account) {
    if (this.userName.equals(account.getUserName()) 
        && this.password.equals(account.getPassword())) {
      return true;
    } else {
      return false;
    }
    
  }
  
}
