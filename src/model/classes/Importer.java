package model.classes;

import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import model.wrapper.NoAppriciateResultException;

public class Importer extends Employee {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private Account account;
  
  public Importer() {
    super();
  }
  
  
  
  public Account getAccount() {
    return account;
  }



  public void setAccount(Account account) {
    this.account = account;
  }



  public ImportReceipt makeImportReceipt(MediaOne store, Supplier supplier, ArrayList<ProductItem> productItems, int boothName, int total) throws NoAppriciateResultException {
    //create new instance of ImportReceipt
    ImportReceipt receipt = new ImportReceipt();
    
    // increase amount of receipt of Salesman 1
    
    int receiptId = store.getAmountOfImportReceipt() + 1;
    
    // set receipt's id
    store.setAmountOfImportReceipt(receiptId);
    receipt.setIdReceipt(receiptId);
    
    // set importer
    receipt.setImporter(this);
    
    // set receipt's supplier
    receipt.setSupplier(supplier);
    
    // set receipt's item
    receipt.setProductItems(productItems);
    
    // set receipt's total
    receipt.setTotal(total);
    
    // set receipt's time
    receipt.setTime(new Date());
    
    store.addAllProductsOfReceipt(receipt);
    store.locateProductItems(receipt, boothName);
    store.getImportReceipts().add(receipt);
    
    // add to Salesman's receipt list    
    return receipt;
  }
  
  public static void insertItemToListItem(ArrayList<ProductItem> listItems, Product product, int amount, int importPrice) throws Exception {
    ProductItem item = new ProductItem(product, amount, importPrice);
    listItems.add(item);
  }
  
  public static void deleteItemFromListItem(ArrayList<ProductItem> listItem, int index) {
    listItem.remove(index);
  }
  
  public String getPosition() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    return b.getString("importer");
  }
  
  
  @Override 
  public int countSalary() {
    return super.countSalary() ;
  }

}
