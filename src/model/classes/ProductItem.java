package model.classes;

import java.io.Serializable;

public class ProductItem implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private Product product;
  private int amount;
  private int price;
  
  public ProductItem(Product product, int amount, int price) {
    this.product = product;
    this.amount = amount;
    this.price = price;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }
  
  public boolean descrease(int value) {
    if (this.amount - value < 0) {
      return false;
    } else {
      this.amount -= value;
      return true;
    }
  }
  
}
