package model.classes;

import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class Salesman extends Employee {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int commission;
  private Account account;
  
  public Salesman() {
    super();
    commission = 0;
   
  }

  public int getCommission() {
    return commission;
  }

  public void setCommission(int commission) {
    this.commission = commission;
  }
  
  @Override 
  public int countSalary() {
    return super.countSalary() + this.commission;
  }
  
  
  
  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  /**
   * Create new instance of ExportReceipt.
   * @param productItems items
   * @param total total price
   * @return new ExportReceipt
   */
  
  public ExportReceipt makeExportReceipt(MediaOne store, ArrayList<ProductItem> productItems, int total) {
    // create new instance of ExportReceipt
    ExportReceipt receipt = new ExportReceipt();
 
    int receiptId = store.getAmountOfExportReceipt() + 1;
    // set idReceipt
    store.setAmountOfExportReceipt(receiptId);
    receipt.setIdReceipt(receiptId);
    
    // set creator
    receipt.setSalesman(this);

    // set item
    receipt.setProductItems(productItems);
    
    // set total
    receipt.setTotal(total);
    
    // set time
    receipt.setTime(new Date());

    store.getExportReceipts().add(receipt);

    store.updateStore(receipt);
    
    return receipt;
  }

  public static void insertItemToListItem(MediaOne store, ArrayList<ProductItem> listItems, int productId, int amount) throws Exception {
    Product product = store.searchProductById(productId);
    ProductItem item = store.searchProductItemByProduct(product);
    if (amount > item.getAmount()) {
      throw new Exception("Invalid id or amount");
    }
    int saleOff = store.getSaleOffPercent(product);
    int exportPrice = ((100 - saleOff) * product.getPrice()) / 100;
    ProductItem exportItem = new ProductItem(product, amount, exportPrice);
    listItems.add(exportItem);
  }
  
  public static void deleteItemFromListItem(ArrayList<ProductItem> listItem, int index) {
    listItem.remove(index);
  }
  
  public String getPosition() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    return b.getString("salesman");
  }
  
 
}
