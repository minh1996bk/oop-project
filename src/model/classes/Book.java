package model.classes;

public class Book extends Product {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String author;
  private String summaryContents;
  private BookGenre bookGenre;
  
  public Book() {
    super();
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getSummaryContents() {
    return summaryContents;
  }

  public void setSummaryContents(String summaryContents) {
    this.summaryContents = summaryContents;
  }

  public BookGenre getBookGenre() {
    return bookGenre;
  }

  public void setBookGenre(BookGenre bookGenre) {
    this.bookGenre = bookGenre;
  }
  
  public boolean checkRequired(String searchPattern, String genre) {
    if (bookGenre.getName().equals(genre)
        && (this.getProductName().contains(searchPattern)
            || this.author.contains(searchPattern)
        )) {
      return true;
    }
    return false;
  }
  
}
