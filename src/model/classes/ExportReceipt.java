package model.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class ExportReceipt implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int idReceipt;
  private Salesman salesman;
  private ArrayList<ProductItem> productItems;
  private Date time;
  private int total;
  
  public ExportReceipt() {
    
  }

  public int getIdReceipt() {
    return idReceipt;
  }

  public void setIdReceipt(int id) {
    this.idReceipt = id;
  }

  public ArrayList<ProductItem> getProductItems() {
    return productItems;
  }

  public void setProductItems(ArrayList<ProductItem> productItems) {
    this.productItems = productItems;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public Salesman getSalesman() {
    return salesman;
  }

  public void setSalesman(Salesman salesman) {
    this.salesman = salesman;
  }

 
  
  
  
  
  
}
