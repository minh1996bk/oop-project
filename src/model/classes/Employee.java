package model.classes;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.Date;
import java.util.ResourceBundle;

public class Employee implements Serializable {
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int idEmployee;
  private String employeeName;
  private Date birthDay;
  private Sex sex;
  private String identification;
  private String phoneNumber;
  private String email;
  private String nativeLand;
  private String address;
  private transient BufferedImage image;
  private int coefficientsSalary;
  private static final int BASIC_SALARY = 1000000;
  private Account account;
  
  public Employee() {
    
  }
  
  
  
  public int getIdEmployee() {
    return idEmployee;
  }


  public void setIdEmployee(int idEmployee) {
    this.idEmployee = idEmployee;
  }


  public String getEmployeeName() {
    return employeeName;
  }


  public void setEmployeeName(String employeeName) {
    this.employeeName = employeeName;
  }


  public Date getBirthDay() {
    return birthDay;
  }


  public void setBirthDay(Date birthDay) {
    this.birthDay = birthDay;
  }


  public Sex getSex() {
    return sex;
  }


  public void setSex(Sex sex) {
    this.sex = sex;
  }


  public String getIdentification() {
    return identification;
  }


  public void setIdentification(String identification) {
    this.identification = identification;
  }


  public String getPhoneNumber() {
    return phoneNumber;
  }


  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }


  public String getEmail() {
    return email;
  }


  public void setEmail(String email) {
    this.email = email;
  }


  public String getNativeLand() {
    return nativeLand;
  }


  public void setNativeLand(String nativeLand) {
    this.nativeLand = nativeLand;
  }


  public String getAddress() {
    return address;
  }


  public void setAddress(String address) {
    this.address = address;
  }

  public int getCommission() {
    return 0;
  }
  
  public BufferedImage getImage() {
    return image;
  }


  public void setImage(BufferedImage image) {
    this.image = image;
  }


  public int getCoefficientsSalary() {
    return coefficientsSalary;
  }


  public void setCoefficientsSalary(int coefficientsSalary) {
    this.coefficientsSalary = coefficientsSalary;
  }


  public static int getBasicSalary() {
    return BASIC_SALARY;
  }
  
  
  
  public Account getAccount() {
    return account;
  }



  public void setAccount(Account account) {
    this.account = account;
  }



  public int countSalary() {
    return this.coefficientsSalary * BASIC_SALARY;
  }
  

  public String getPosition() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    return b.getString("employee");
  }
}
