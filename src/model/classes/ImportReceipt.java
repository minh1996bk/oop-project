package model.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class ImportReceipt implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int idReceipt;
  private Importer importer;
  private Supplier supplier;
  private ArrayList<ProductItem> productItems;
  private Date time;
  private int total;
  
  public ImportReceipt() {
    
  }

  public int getIdReceipt() {
    return idReceipt;
  }

  public void setIdReceipt(int idReceipt) {
    this.idReceipt = idReceipt;
  }

  public Supplier getSupplier() {
    return supplier;
  }

  public void setSupplier(Supplier supplier) {
    this.supplier = supplier;
  }

  public ArrayList<ProductItem> getProductItems() {
    return productItems;
  }

  public void setProductItems(ArrayList<ProductItem> productItems) {
    this.productItems = productItems;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public Importer getImporter() {
    return importer;
  }

  public void setImporter(Importer importer) {
    this.importer = importer;
  }
  
  
  
  
}
