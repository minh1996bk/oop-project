package model.classes;

public class Film extends Product {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String actorNames;
  private String summaryContents;
  private FilmGenre filmGenre;
  
  public Film() {
    
  }

  public String getActorNames() {
    return actorNames;
  }

  public void setActorNames(String actorNames) {
    this.actorNames = actorNames;
  }

  public String getSummaryContents() {
    return summaryContents;
  }

  public void setSummaryContents(String summaryContents) {
    this.summaryContents = summaryContents;
  }

  public FilmGenre getFilmGenre() {
    return filmGenre;
  }

  public void setFilmGenre(FilmGenre filmGenre) {
    this.filmGenre = filmGenre;
  }
  
  public boolean checkRequired(String searchPattern, String genre) {
    if (filmGenre.getName().equals(genre)
        && (this.getProductName().contains(searchPattern)
            || this.actorNames.contains(searchPattern))) {
      return true;
    }
    return false;
  }
  
  
  
}
