package model.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Booth implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int name;
  private ArrayList<ProductItem> productItems;
  
  public Booth(int name) {
    this.name = name;
    this.productItems = new ArrayList<>();
  }

  public int getName() {
    return name;
  }

  public void setName(int name) {
    this.name = name;
  }

  public ArrayList<ProductItem> getProductItems() {
    return productItems;
  }

  public void setProducts(ArrayList<ProductItem> productItem) {
    this.productItems = productItem;
  }
  
  public LinkedHashMap<ProductItem, Booth> searchProductItem(String searchPattern, String genre) {
    LinkedHashMap<ProductItem, Booth> map = new LinkedHashMap<>();
    for (ProductItem item : productItems) {
      if (item.getProduct().checkRequired(searchPattern, genre)) {
        map.put(item, this);
      }
    }
    return map;
  }
}
