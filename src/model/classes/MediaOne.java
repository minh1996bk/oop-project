package model.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import model.wrapper.InvalidExportReceiptIdException;
import model.wrapper.NoAppriciateResultException;

public class MediaOne implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String name;
  private ArrayList<Cost> costs;
  private ArrayList<OtherCost> otherCosts;
  private transient ArrayList<Product> products;
  private ArrayList<BookGenre> bookGenres;
  private ArrayList<FilmGenre> filmGenres;
  private ArrayList<MusicGenre> musicGenres;
  private transient ArrayList<Employee> employees;
  private ArrayList<Supplier> suppliers;
  private transient ArrayList<Booth> booths;
  private transient ArrayList<ImportReceipt> importReceipts;
  private transient ArrayList<ExportReceipt> exportReceipts;
  private Account rootAccount;
  private LinkedHashMap<Product, Integer> saleOffList;

  private int amountOfImportReceipt;
  private int amountOfExportReceipt;
  private int amountOfBooth;
  private int amountOfEmployee;
  private int amountOfProduct;
  private int amountOfSupplier;
  
  
  public MediaOne() {
    ResourceBundle b = ResourceBundle.getBundle("file.resourcebundle.Label");
    this.name = b.getString("mediaOne");
    this.costs = new ArrayList<>();
    this.otherCosts = new ArrayList<>();
    this.bookGenres = new ArrayList<>();
    this.filmGenres = new ArrayList<>();
    this.musicGenres = new ArrayList<>();
    this.suppliers = new ArrayList<>();
    this.saleOffList = new LinkedHashMap<>();
    this.rootAccount = new Account("root", "root");
  }

  public ArrayList<OtherCost> getOtherCosts() {
    return otherCosts;
  }

  public void setOtherCosts(ArrayList<OtherCost> otherCosts) {
    this.otherCosts = otherCosts;
  }

  public ArrayList<Product> getProducts() {
    return products;
  }

  public void setProducts(ArrayList<Product> products) {
    this.products = products;
  }

  public ArrayList<BookGenre> getBookGenres() {
    return bookGenres;
  }

  public void setBookGenres(ArrayList<BookGenre> bookGenres) {
    this.bookGenres = bookGenres;
  }

  public ArrayList<FilmGenre> getFilmGenres() {
    return filmGenres;
  }

  public void setFilmGenres(ArrayList<FilmGenre> filmGenres) {
    this.filmGenres = filmGenres;
  }

  public ArrayList<MusicGenre> getMusicGenres() {
    return musicGenres;
  }

  public void setMusicGenres(ArrayList<MusicGenre> musicGenres) {
    this.musicGenres = musicGenres;
  }

  public ArrayList<Employee> getEmployees() {
    return employees;
  }

  public void setEmployees(ArrayList<Employee> employees) {
    this.employees = employees;
  }

  public ArrayList<Booth> getBooths() {
    return booths;
  }

  public void setBooths(ArrayList<Booth> booths) {
    this.booths = booths;
  }

  public ArrayList<ImportReceipt> getImportReceipts() {
    return importReceipts;
  }

  public void setImportReceipts(ArrayList<ImportReceipt> receipts) {
    this.importReceipts = receipts;
  }
  
  public ArrayList<ExportReceipt> getExportReceipts() {
    return this.exportReceipts;
  }
  
  public void setExportReceipts(ArrayList<ExportReceipt> receipts) {
    this.exportReceipts = receipts;
  }
  
  
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArrayList<Supplier> getSuppliers() {
    return suppliers;
  }

  public void setSuppliers(ArrayList<Supplier> suppliers) {
    this.suppliers = suppliers;
  }

  public int getAmountOfImportReceipt() {
    return amountOfImportReceipt;
  }

  public void setAmountOfImportReceipt(int amountOfReceipt) {
    this.amountOfImportReceipt = amountOfReceipt;
  }

  public int getAmountOfExportReceipt() {
    return this.amountOfExportReceipt;
  }
  
  public void setAmountOfExportReceipt(int amount) {
    this.amountOfExportReceipt = amount;
  }
  public int getAmountOfBooth() {
    return amountOfBooth;
  }

  public void setAmountOfBooth(int amountOfBooth) {
    this.amountOfBooth = amountOfBooth;
  }

  public int getAmountOfEmployee() {
    return amountOfEmployee;
  }

  public void setAmountOfEmployee(int amountOfEmployee) {
    this.amountOfEmployee = amountOfEmployee;
  }

  public int getAmountOfProduct() {
    return amountOfProduct;
  }

  public void setAmountOfProduct(int amountOfProduct) {
    this.amountOfProduct = amountOfProduct;
  }
  
  public int getAmountOfSupplier() {
    return amountOfSupplier;
  }

  public void setAmountOfSupplier(int amountOfSupplier) {
    this.amountOfSupplier = amountOfSupplier;
  }
  
  public Account getRootAccount() {
    return rootAccount;
  }

  public void setRootAccount(Account rootAccount) {
    this.rootAccount = rootAccount;
  }
  


  public ArrayList<Cost> getCosts() {
    return costs;
  }

  public void setCosts(ArrayList<Cost> costs) {
    this.costs = costs;
  }


  public LinkedHashMap<Product, Integer> getSaleOffList() {
    return saleOffList;
  }

  public void setSaleOffList(LinkedHashMap<Product, Integer> saleOffList) {
    this.saleOffList = saleOffList;
  }
  // Methods below.

  // Below are Processes related to Employee

  public void addNewEmployee(Employee employee) {
    // increase amount of employee 1
    this.amountOfEmployee ++;
    
    // set id for employee
    employee.setIdEmployee(amountOfEmployee);
    
    // add this employee to employee list
    this.employees.add(employee);
  
  }
  
  /**
   * Seek for employee by idEmployee.
   * @param idEmployee id of Employee who is searched
   * @return instance of Employee
   * @throws NoAppriciateResultException if no employee with that id
   */
  
  public Employee searchEmployee(int idEmployee) throws NoAppriciateResultException {
    for (Employee employee : this.employees) {
      if (employee.getIdEmployee() == idEmployee) {
        return employee;
      }
    }
    throw new NoAppriciateResultException();
  }
  
  public ArrayList<Employee> searchEmployee(String name) throws NoAppriciateResultException {
    ArrayList<Employee> results = new ArrayList<>();
    for (Employee employee : this.employees) {
      if (employee.getEmployeeName().contains(name)) {
        results.add(employee);
      }
    }
    if (results.isEmpty()) {
      throw new NoAppriciateResultException();
    } else {
      return results;
    }
  }
  
  // Above are Processes related to Employee
  
  // Below are Processes related to Product
  public void addProductToSaleOffList(Product product, int saleOffPercent) {
    this.saleOffList.put(product, saleOffPercent);
  }
  
  public void addNewSupplier(Supplier supplier) {
    this.amountOfSupplier ++;
    supplier.setIdSupplier(this.amountOfSupplier);
    this.suppliers.add(supplier);
  }
  
  public void addNewProduct(Product product) {
    this.amountOfProduct ++;
    product.setIdProduct(amountOfProduct);
    this.products.add(product);
  }
  
  public void addNewFilmGenre(FilmGenre genre) {
    this.filmGenres.add(genre);
  }
  
  public void addNewMusicGenre(MusicGenre genre) {
    this.musicGenres.add(genre);
  }
  
  public void addNewBookGenre(BookGenre genre) {
    this.bookGenres.add(genre);
  }
  
  public void addNewBooth() {
    this.amountOfBooth ++;
    this.booths.add(new Booth(this.amountOfBooth));
  }
  
  public void addAllProductsOfReceipt(ImportReceipt receipt) {
    ArrayList<ProductItem> items = receipt.getProductItems();
    for (ProductItem item : items) {
      this.addNewProduct(item.getProduct());
    }
  }

  public void locateProductItems(ImportReceipt receipt, int boothName) throws NoAppriciateResultException {
    ArrayList<ProductItem> items = receipt.getProductItems();
    Booth booth = this.searchBooth(boothName);
    for (ProductItem item : items) {
      booth.getProductItems().add(item);
    }
  }
  
  /**
   * Seek for ExportReceipt by idReceipt.
   * @param idReceipt receipt's id 
   * @return Receipt with appriciate id
   * @throws InvalidExportReceiptIdException if idReceipt in wrong format
   * @throws NoAppriciateResultException if no appriciate result
   */
  
  public ExportReceipt searchExportReceipt(int idReceipt) throws InvalidExportReceiptIdException, NoAppriciateResultException {
  
    
    // get receipt that id equals idReceipt
   
    for (ExportReceipt receipt : exportReceipts) {
      if (receipt.getIdReceipt() == idReceipt) {
        return receipt;
      }
    }
    
    // thow NoAppriciateResultException if no appriciate result
    throw new NoAppriciateResultException();
  }
  
  /**
   * Seek for importReceipt by id.
   * @param idReceipt of ImportReceipt
   * @return instance of ImportReceipt
   * @throws NoAppriciateResultException if no appriciate result
   */
  
  public ImportReceipt searchImportReceipt(int idReceipt) throws NoAppriciateResultException {
    for (ImportReceipt receipt : this.importReceipts) {
      if (receipt.getIdReceipt() == idReceipt) {
        return receipt;
      }
    }
    throw new NoAppriciateResultException();
  }
  
  public ImportReceipt searchImportReceiptByProduct(int idProduct) throws NoAppriciateResultException {
    for (ImportReceipt receipt : this.importReceipts) {
      for (ProductItem item : receipt.getProductItems()) {
        if (item.getProduct().getIdProduct() == idProduct) {
          return receipt;
        }
      }
    }
    throw new NoAppriciateResultException();
  }
  /**
   * Seek for product by idProduct.
   * @param idProduct product's id
   * @return product's  instance
   * @throws NoAppriciateResultException if no appriciate result
   */
  
  public Product searchProductById(int idProduct) throws NoAppriciateResultException {
    for (Product product : this.products) {
      if (product.getIdProduct() == idProduct) {
        return product;
      }
    }
    throw new NoAppriciateResultException();
  }
  
  public ProductItem searchProductItemByProduct(Product product) throws NoAppriciateResultException {
    for (Booth booth : this.booths) {
      ArrayList<ProductItem> items = booth.getProductItems();
      for (ProductItem item : items) {
        if (item.getProduct().getIdProduct() == product.getIdProduct()) {
          return item;
        }
      }
    }
    throw new NoAppriciateResultException();
  }
  
  public LinkedHashMap<ProductItem, Booth> searchProductItemAndCorrespondBooth(String searchPattern, String genre) {
    LinkedHashMap<ProductItem, Booth> map = new LinkedHashMap<>();
    for (Booth booth : booths) {
      map.putAll(booth.searchProductItem(searchPattern, genre));
    }
    return map;
  }
  public Booth searchBoothItemByProduct(Product product) throws NoAppriciateResultException {
    for (Booth booth : this.booths) {
      ArrayList<ProductItem> items = booth.getProductItems();
      for (ProductItem item : items) {
        if (item.getProduct().getIdProduct() == product.getIdProduct()) {
          return booth;
        }
      }
    }
    throw new NoAppriciateResultException();
  }
  
  public void updateStore(ExportReceipt receipt) {
    ArrayList<ProductItem> items = receipt.getProductItems();
    boolean check = false;
    for (ProductItem item : items) {
//      try {
//        ProductItem oldItem = this.searchProductItemByProduct(item.getProduct());
//        oldItem.descrease(item.getAmount());
//        if (oldItem.getAmount() == 0) {
//          
//        }
//      } catch (NoAppriciateResultException e) {
//        // TODO Auto-generated catch block
//        e.printStackTrace();
//      }
      
      for (Booth booth : this.booths) {
        for (ProductItem oldItem : booth.getProductItems()) {
          if (item.getProduct().getIdProduct() == oldItem.getProduct().getIdProduct()) {
            oldItem.descrease(item.getAmount());
            if (oldItem.getAmount() == 0) {
              booth.getProductItems().remove(oldItem);
            }
            check = true;
            break;
          }
        }
        if (check) {
          break;
        }
      }
    }
  }
  /**
   * Seek for Booth by name.
   * @param boothName booth's name
   * @return appriciate booth
   * @throws NoAppriciateResultException if now appriciate Booth
   */
  
  public Booth searchBooth(int boothName) throws NoAppriciateResultException {
    for (Booth booth : this.booths) {
      if (booth.getName() == boothName) {
        return booth;
      }
    }
    throw new NoAppriciateResultException();
  }
  
  public int getSaleOffPercent(Product product) {
    for (Map.Entry<Product, Integer> pair : this.saleOffList.entrySet()) {
      if (pair.getKey().getIdProduct() == product.getIdProduct()) {
        return pair.getValue();
      }
    }
    return 0;
  }
  // Above are Process related to Product
  
  /**
   * Pay for new MediaOne's cost.
   * @param otherCost new other cost's instance
   */
  
  public void addNewCost(Cost cost) {
    this.costs.add(cost);
  }
  
  public void payOtherCost(OtherCost otherCost) {
    this.otherCosts.add(otherCost);
  }
  
  
  // Below are MediaOne's statistics
  
  /**
   * Take a statistic for MediaOne's other cost.
   * @param fromDate from date 
   * @param toDate to date
   * @return other costs
   */
  
  public ArrayList<OtherCost> takeStatisticForOtherCost(Date fromDate, Date toDate) {
    ArrayList<OtherCost> results = new ArrayList<>();
    for (OtherCost cost : this.otherCosts) {
      if (cost.getTime().after(fromDate) 
          && cost.getTime().before(toDate)) {
        results.add(cost);
      }
    }
    return results;
  }
  
  public int countImportTotal(Date fromDate, Date toDate) {
    int count = 0;
    for (ImportReceipt receipt : this.importReceipts) {
      if (receipt.getTime().after(fromDate)
          && receipt.getTime().before(toDate)) {
        count += receipt.getTotal();
      }
    }
    return count;
  }
  
  public int countExportTotal(Date fromDate, Date toDate) {
    int count = 0;
    for (ExportReceipt receipt : this.exportReceipts) {
      if (receipt.getTime().after(fromDate)
          && receipt.getTime().before(toDate)) {
        count += receipt.getTotal();
      }
    }
    return count;
  }
  
  public String[] getMusicGenreToString() {
    String[] array = new String[musicGenres.size()];
    for (int i = 0; i < musicGenres.size(); i ++) {
      array[i] = musicGenres.get(i).getName();
    }
    return array;
  }
  
  public String[] getBookGenreToString() {
    String[] array = new String[bookGenres.size()];
    for (int i = 0; i < bookGenres.size(); i ++) {
      array[i] = bookGenres.get(i).getName();
    }
    return array;
  }
  
  public String[] getFilmGenreToString() {
    String[] array = new String[filmGenres.size()];
    for (int i = 0; i < filmGenres.size(); i ++) {
      array[i] = filmGenres.get(i).getName();
    }
    return array;
  }
  
  public int countSalary() {
    int count = 0;
    for (Employee employee : employees) {
      count += employee.countSalary();
    }
    return count;
  }
  
  public Employee getEmployeeByAccount(Account account) throws Exception {
    for (Employee employee : employees) {
      if (employee.getAccount().equals(account)) {
        return employee;
      }
    }
    throw new Exception("Wrong account");
  }

}
