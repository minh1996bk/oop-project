package model.classes;

import java.io.Serializable;

public class Product implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int idProduct;
  private String productName;
  private int price;
  
  public Product() {
    
  }

  public int getIdProduct() {
    return idProduct;
  }

  public void setIdProduct(int idProduct) {
    this.idProduct = idProduct;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }
  
  public boolean checkRequired(String searchPattern, String genre) {
    return false;
  }
  
 
  
}
