package model.classes;

import java.io.Serializable;

public class Supplier implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int idSupplier;
  private String supplierName;
  private String address;
  private String email;
  private String phoneNumber;
  
  public Supplier() {
    
  }

  public int getIdSupplier() {
    return idSupplier;
  }

  public void setIdSupplier(int idSupplier) {
    this.idSupplier = idSupplier;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }
  
  
}
