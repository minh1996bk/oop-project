package model.classes;

import java.io.Serializable;

public class Cost implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String name;
  
  public Cost(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  
}
